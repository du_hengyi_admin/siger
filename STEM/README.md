#[I3895J](https://gitee.com/flame-ai/siger/issues/I3895J) 《[向STEM UK社区学习“志愿者大使”计划](https://gitee.com/flame-ai/siger/issues/I3895J)》中介绍了STEM.org.uk 之于SIGer的意义——作为科普志愿者培训的教材。建立本目录，旨在组织志愿者对STEM UK在futurelearn上的课程进行本地化，以形成适合中国志愿者队伍建设的内容。也是SIGer的能力建设的重要成果。

而它的缘起则是 石榴派国际漂流 到英国树莓派基金会，因缘际会会以《1980年代英国人造的家用电脑》的译文作为主要内容，只能说任何结果都是之前的发愿注定的，享受当下的学习成果吧。作为完整的译文留待 SIGer 编委会更多同学的参与，本次发布只限 2021年11月27日 前，也就是 "[福祺](https://gitee.com/blesschess)" 抵达英国，树莓派基金会签收为阶段。

- [赴英全程](https://gitee.com/blesschess/luckystar/issues/I3YL4C#note_7427390_link) :pray:
- [The Computers that Made Britain](https://gitee.com/blesschess/luckystar/issues/I4IYBI) 译文工作贴

于此同时，译文全文多次提及的计算机博物馆，则由另一位老师多年前写过游记，前几日还在同他商讨将文章归并到哪个栏目，无疑Stem最合适不过，因为这也是我们相识的时间点，让我重回曾经如火如荼的 创客年代，只希望能更长久，更长青。:pray:

- [无为堂记：计算机历史博物馆](https://gitee.com/yuandj/siger/issues/I4JITT)

FlameAI
ShiliuPi
袁德俊
2021-12-06

---

[![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/112359_7d8f56f1_5631341.jpeg "封面副本999.jpg")](https://images.gitee.com/uploads/images/2021/1122/112344_7c70bb4a_5631341.jpeg)

> [选定 202111119 作为期刊日期，也是一个纪念。历史总是惊人的相似，这是轮回！](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7490559_link) :pray: 