[![输入图片说明](https://images.gitee.com/uploads/images/2021/1213/054040_2f98cf7b_5631341.jpeg "Zero2副本3.1.999.jpg")](https://images.gitee.com/uploads/images/2021/1213/054023_8aaef31e_5631341.jpeg)

### [I got the Raspberry Pi Zero 2 already. :D](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3)

- 1st step is to install Shiliu Pi 99 on it.
  1. https://www.raspberrypi.org/
  2. [Raspberry Pi Zero 2 W](https://www.raspberrypi.com/products/raspberry-pi-zero-2-w/)
  3. [Getting started guide](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780631_link)
  4. [使用 RP3 / RP4 的现成 SD, pios 32bit 直接启动。](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780644_link)

     > 但是没有 HDMI 输出。用 WAN LAN 连接 可以 IP 访问并登录，说明 系统100%兼容。也就是说 树莓派BASEED 的 石榴派 完全兼容。
     - [ShiliuPi 私有云 运行正常！CCB 云棋盘正常！](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780731_link)

- [java -jar Desktop/LuckyLudii3.3.jar error](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780738_link)
  - [ctrl-c 终止 JAVA 线程。](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780755_link)
  - [java.lang.NullPointerException](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780770_link)

- [compare: ](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780830_link)
  | mem | P3B+ | PI ZERO 2W | PI 4B+ 8G | QUARK 512M |
  |---|---|---|---|---|
  | Total RAM: | 910 MB | 365 MB | 7757 MB | 491 MB |
  | Used RAM: | 302 MB | 192 MB | 1050 MB | 172 MB |
  | Cached RAM: | 312 MB | 100 MB | 980 MB | 275 MB |
  | Buffers: | 30 MB | 17 MB | 51 MB | 15 MB |

  - [pi@raspberrypi:~ $ java -jar -Xms64m -Xmx128m Desktop/LuckyLudii3.3.jar](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780846_link)
    - [Exception in thread "AWT-EventQueue-0" java.lang.OutOfMemoryError: Java heap space](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780847_link)

- [java -jar -Xms64m -Xmx192m Desktop/LuckyLudii3.3.jar](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780848_link)
  Compiled Surakarta successfully.

  - [java -jar -Xms64m -Xmx160m Desktop/LuckyLudii3.3.jar](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780854_link)

    - Compiled Surakarta successfully.
    - [Compiled Jiu Zi Xian Qi successfully.](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780861_link)
    - [Compiled Lucky Sudoku successfully.](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780862_link)
    - [MobaXterm Personal Edition v21.1 Last login: Fri Dec 10 02:31:31 2021](https://gitee.com/shiliupi/OpenEyeTap/issues/I4LVS3#note_7780863_link)

[![输入图片说明](https://images.gitee.com/uploads/images/2021/1213/045309_b8502ead_5631341.jpeg "eyetap999.jpg")](https://images.gitee.com/uploads/images/2021/1213/045347_f0c723cb_5631341.jpeg)

### [OpenEyeTap：基于树莓派的开源AR智能眼镜](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE)

>  _MAKER：SteveMann／译：趣无尽 Cherry（转载请注明出处）_ 

我们这一款EyeTap智能眼镜，使用3D打印组件，内置光学微型显示器，微型摄像头和带wifi功能的树莓派ZeroW。具有电子记录功能（类似车载记录仪）和快照功能。以下是项目团队对该项目的说明。

> 欢迎各位了解Open EyeTap项目！我们是一个具有雄心壮志的创作团队，致力于打造世界上最灵活的智能眼镜和可穿戴增强现实的社区。我们希望提供一个框架使AR技术可以蓬勃发展。我们希望与世界各地的设计师分享我们的EyeTap。作为一个社区，我们可以共同改进这种开源技术。

> 我们在这个项目中的主要目标是简化EyeTap的构建。我们希望它能帮助您建立自己的体系和减少进入AR领域的障碍。也希望你会找到有趣的功能和设计（也许是符合你生活方式的特定功能），都可以添加和分享到我们的网站：openeyetap.com！我们相信，作为一个社区，我们都可以成为开发第一波开源增强现实式眼镜不可或缺的有力臂膀。

下面我们详细介绍这款造价不到200美元的EyeTap的DIY步骤，你就可以打造同款了。简而言之，我们将使用3D打印组件，内置光学微型显示屏，微型摄像头和带wifi功能的树莓派 Zero W。我们目前开发了一种可以用EyeTap运行的电子记录功能（类似车载记录仪），更多其他模块和功能即将推出。

- 功能1：记录功能（Dash-camera） +快照功能
- 什么是纪录功能？
- 步骤：
  1. 项目所需材料
  2. 3D打印EyeTap部件
  3. 组装EyeTap框架
  4. 组装微型显示屏
  5. 组装鼻托模块
  6. 使用微型摄像头构建树莓派模块
  7. 将微型显示屏连接到树莓派ZeroW
  8. 将按钮连接到树莓派ZeroW
  9. 集成硬件和其他机械零件
  10. 软件＃1 纪录功能(dashcam) +快照功能
  11. 启动EyeTap！

文章标题：OpenEyeTap：基于树莓派的开源AR智能眼镜 - 树莓派实验室
固定链接：https://shumeipai.nxez.com/2018/05/09/openeyetap-3d-printed-programmable-smart-glass.html

- 本项目所需的资源可在项目文件库中找到：【[笔记](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7555535_link)】

  - http://make.quwj.com/project/45 【[笔记](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7565781_link)】
  - via https://www.instructables.com/OpenEyeTap-3D-Printed-Programmable-Smart-Glass/

### [让我们愉快的学习树莓派！](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7560088_link):pray:

- [用树莓派改造猫厕所：智能通风，告别异味](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7559434_link) 图[1](https://images.gitee.com/uploads/images/2021/1128/010217_110c4dae_5631341.png) / [2](https://images.gitee.com/uploads/images/2021/1128/010230_d20ace7a_5631341.png) / [3](https://images.gitee.com/uploads/images/2021/1128/010240_6128c15e_5631341.png) / [4](https://images.gitee.com/uploads/images/2021/1128/010251_39ec46ab_5631341.png) / [5](https://images.gitee.com/uploads/images/2021/1128/010310_30bc6e12_5631341.png) / [6](https://images.gitee.com/uploads/images/2021/1128/010322_f074721e_5631341.png)

  - NAS 私有云和 KODI 软硬集成的套件  
    [NAS以及微力还有SMB的默认账户密码呢？总不能我拆开镜像查看吧？](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7560136_link)
  - [上一个：启用树莓派 CM4 CM4IO 的双路摄像头](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7560110_link)  
    下一个：工业树莓派结合USB摄像头实现远程网络监控

- [BING 树莓派的AR实现方案](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7565954_link)

  - [OpenEyeTap：基于树莓派的开源AR智能眼镜](https://shumeipai.nxez.com/2018/05/09/openeyetap-3d-printed-programmable-smart-glass.html) | 树莓派实验室
  - [AR眼镜中的光学显示方案原理及其工艺全解析](https://vr.sina.com.cn/news/hz/2020-06-17/doc-iirczymk7483042.shtml)【[笔记](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7569349_link)】
  - [第二课：基于树莓派的10个经典项目(树莓派能做什么)_下家山](https://blog.csdn.net/qq_27320195/article/details/105444549) ... 【[笔记](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7566352_link)】
  - [上面的视频是收费，没有找到原来的，找到B站一个UP主自己推荐的。](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7567230_link)（极趣科学）
    - [400元做一个13.3寸笔记本电脑！](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7568199_link)
    - ["SnookMe" Interactive Pool Training System，和“台球打不好”说再见](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7568573_link)
    - [这就是前面两个项目的出处，学习啦。](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7568601_link)

- [树莓派开发](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7569440_link)

- [放弃MacBook Pro，用8GB的树莓派4工作一天，是这样的感受！](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7569605_link)
  - [这个我看到过！符合我的需求，石榴派做过验证！](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7569606_link)  
    而多媒体，可以借助 UBUNTU, 非常NICE. 我个人对 PI4 非常看好，而且很快它就会 PI5 PI6 PI...
    > 如果你对计算机的使用主要是浏览器、代码编辑器、命令行（如后端 Web 开发、基础架构开发、写博客之类），那 8GB 内存的树莓派 4 完全够用了。即使你打开了一堆标签页，Chromium 也能运行良好。

- [bing 树莓派的双目的实现方案](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7569652_link)

  - [树莓派驱动双目摄像头_野犬1998的博客-CSDN博客_树莓派](https://blog.csdn.net/qq_42319367/article/details/88098729) ...【[笔记](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7569670_link)】
  - [双目测距程序讲解 OpenCV python 树莓派 BM匹配算法_哔哩](https://www.bilibili.com/video/BV1Kb41147Fe) ...
  - [基于树莓派的智能小车：自动避障、实时图像传输、视觉车道](https://www.cnblogs.com/MingruiYu/p/12184953.html) ...
  - [树莓派介绍以及FAQ](https://shumeipai.nxez.com/intro-faq) | 树莓派实验室 - NXEZ
  - [树莓派上的立体视觉和深度知觉](https://shumeipai.nxez.com/2014/11/25/stereo-depth-perception-raspberry-pi.html) | 树莓派实验室 - NXEZ 【[笔记](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7569729_link)】

- [bing OpenEyeTap AR raspberry pi](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7569811_link)

  - [OpenEyeTap: 3D Printed & Programmable Smart Glass](https://www.instructables.com/OpenEyeTap-3D/) : …
  - [OpenEyeTap：基于树莓派的开源AR智能眼镜](https://shumeipai.nxez.com/2018/05/09/openeyetap-3d-printed-programmable-smart-glass.html) | 树莓派实验室
  - [Augmented Reality Eyeglass With Thermal Vision: Build](https://gitee.com/link?target=https%3A%2F%2Fwww.instructables.com%2FAugmented-Reality-Eyeglass-With-Thermal-Vision-Bui%2F) … 【[笔记](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7569817_link)】
  - [基于树莓派和 Vufine 屏的谷歌眼镜](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7570478_link)

- https://www.instructables.com/ [autodesk](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7570072_link)

  - [WHY PUBLISH AN INSTRUCTABLE?](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7570120_link)
  - [Site Map](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7570145_link) | [HELP CENTER](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7570155_link) | [CONTACT](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7570171_link)
  - [LEGAL NOTICES & TRADEMARKS](https://www.autodesk.com/company/legal-notices-trademarks/copyright-information/notice-and-procedure-for-making-claims-of-copyright-infringement) 【[笔记](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7570213_link)】

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/202329_9574efa0_5631341.jpeg "在这里输入图片标题")

### [Augmented Reality Eyeglass With Thermal Vision: Build Your Own Low-cost Raspberry Pi EyeTap](https://www.instructables.com/Augmented-Reality-Eyeglass-With-Thermal-Vision-Bui/)

[![输入图片说明](https://images.gitee.com/uploads/images/2021/1213/050203_1cac5749_5631341.png "屏幕截图.png")](https://gitee.com/shiliupi/OpenEyeTap/issues/I4JVUE#note_7569817_link)

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/194325_d0350e44_5631341.gif "在这里输入图片标题") 

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/210142_876ef8d0_5631341.gif "在这里输入图片标题")

(The above [metavision](http://wearcam.org/kineveillance.pdf) photograph accurately records the [sightfield](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.670.5305&rep=rep1&type=pdf) of the [EyeTap](http://www.eyetap.org/) Digital Eye Glass).

Build your own low-cost open-source EyeTap eyeglass: the OpenEyeTap project.

Recognize faces, overlay augmented reality content, etc., using the visible light camera, and, optionally, add also a thermal camera... Now you can walk around your house and see where the insulation is failing, see where heat's leaking out, see how the furnace airflow is going through the ductwork and into the house, see which pipes are going to freeze and burst first, and you can even go to a public place and see who's packing heat.

The EyeTap was invented by Steve Mann, and made better by a team of really smart superstar students.

First let's acknowledge the great students that made this work what it is.

Left-to-right: Alex Papanicolaou, Bryan Leung, Cindy Park, Francisco Cendana; Jackson Banbury; Ken Yang; Hao Lu; Sen Yang ([link to full resolution portraits](http://wearcam.org/instructables/EyeTap_instructable_portraits/)). Not pictured: Audrey Hu; Sarang Nerkar; Jack Xie; Kyle Simmons.

The EyeTap has the following functions:

- Live streaming (lifeglogging);
- VMP (Visual Memory Prosthetic);
- PSD (Personal Safety Device), like an automobile "dashcam" or a department store's surveillance camera;
- Thermal vision: see in complete darkness and find suspects... see who has a concealed weapon;
- Machine learning to sense emotions (e.g. is the person hiding the gun angry);
- Many more functions will be added shortly;
- We hope to build a community of users who can also add to the OpenEye project.

Historical notes: The EyeTap wearable computing project dates back to the 1970s and early 1980s and was brought to MIT to found the MIT wearable computing project in the early 1990s ( http://wearcam.org/nn.htm ).

Here my team of students and we present an opensource eyetap you can easily make at home using a 3d printer.

There have been a number of "copycats" making commercial variations of the device, but with significant design flaws (not to mention the lack of an open ethos that would allow the community to correct those design flaws).

There are 3 fundamental principles that an augmented reality glass needs to uphold:

- Space: the visual content needs to be able to be spatially aligned. This is done by satisfying the collinearity criterion;
- Time: the visual content needs to be able to be temporally aligned; feedback delayed is feedback denied;
- Tonality: the visual content needs to be tonally aligned (photoquantigraphic alignment). This is what led to the invention of HDR as a way of helping people see. [[Quantigraphic camera provides HDR eyesight from Father of AR](https://www.slashgear.com/quantigraphic-camera-promises-hdr-eyesight-from-father-of-ar-12246941/), Chris Davies, Slashgear, 2012sep12]

> Step 1: The 3 Fundamental Principals of AR: Why the Market Has Failed to Deliver!
> Step 2: List of Components
> Step 3: 3D Print and Assemble the EyeTap Design
> Step 4: The Code for Thermal Camera ... Lifeglogging
> Step 5: Other Applications
> Step 6: Have Fun and Share Your Work With Others...

[![输入图片说明](https://images.gitee.com/uploads/images/2021/1213/052019_a7b8d5cc_5631341.png "屏幕截图.png")](https://www.instructables.com/Augmented-Reality-Eyeglass-With-Thermal-Vision-Bui/)

（上面的元视觉照片准确地记录了 EyeTap 数字眼镜的视野。）  
 **创建你自己的低成本开源 EyeTap 眼镜: OpenEyeTap 项目。** 

识别人脸，覆盖增强现实内容，等等，使用可见光相机，并可选地，也添加一个热相机…现在你可以走在你的房子,看到绝缘失败,看到热泄漏的地方,看到炉内气流通过管道系统和进了屋子,看到哪些管道冻结和破裂,甚至可以去公共场所,看看谁的包里的热源。

EyeTap 由史蒂夫·曼恩 (Steve Mann) 发明，一群非常聪明的超级明星学生把它做得更好。

首先让我们感谢那些让这一切成为现实的优秀学生。

从左到右: Alex Papanicolaou, Bryan Leung, Cindy Park, Francisco Cendana; 杰克逊班伯里; 肯·杨; 郝陆;杨森(链接到全分辨率照片)。未摄: 胡黛丽; Sarang Nerkar; 杰克谢; 凯尔·西蒙斯。

 **EyeTap有以下功能:** 

- 直播(lifeglogging);
- VMP (视觉记忆假体);
- PSD (个人安全装置)，比如汽车的“行车记录仪”或百货公司的监控摄像头;
- 热视觉:在完全黑暗中看到并找到嫌疑人……看谁有暗藏的武器;
- 通过机器学习来感知情绪(例如，藏枪的人生气了吗);
- 不久将添加更多的功能;
- 我们希望建立一个用户社区，他们也可以加入OpenEye项目。

历史笔记: EyeTap 可穿戴计算项目可以追溯到20世纪70年代和80年代初，并在90年代初被带到麻省理工学院，创立了麻省理工学院可穿戴计算项目 (http://wearcam.org/nn.htm)。

在这里，我的学生团队和我们展示了一个你可以在家里用3d打印机轻松制作的开源眼镜。

有许多“模仿者”在制作这款设备的商业变体，但都存在重大的设计缺陷(更不用说缺乏让社区纠正这些设计缺陷的开放精神)。增强现实玻璃需要遵循3个基本原则:

- 空间:视觉内容需要能够在空间上对齐。这是通过满足共线性准则来实现的;
- 时间:视觉内容需要能够在时间上对齐;反馈延迟即反馈拒绝;
- 调性:视觉内容需要调性对齐(光量化对齐)。这导致了HDR的发明，作为一种帮助人们看东西的方式。[量化相机提供HDR视力的父亲的AR，克里斯戴维斯，Slashgear, 2012sep12]

> 第一步: AR的3个基本原则:为什么市场无法交付!
> 第二步: 组件列表
> 第三步: 3D打印和组装EyeTap设计
> 第四步: 热感相机的代码… Lifeglogging
> 第五步: 其他应用
> 第六步: 玩得开心，和别人分享你的工作……

---

### [Steve Mann](http://wearcam.org/nn.htm) 
was recognized as "The Father of The Wearable Computer", IEEE ISSCC, Feb. 7th, 2000.

<p>
Nicholas Negroponte also recognized his role in founding the field of wearable
technologies:

<h2>Interview with Nicholas Negroponte, Director of MIT Media Lab,
describing how Steve Mann founded the MIT Wearable Computing Project,
as its first member</h2>
<blockquote>
<strong>
      "Steve Mann ...  brought with him an idea... And when he arrived here
       a lot of people sort of said wow this is very interesting... I think it's
       probably one of the best examples we have of where somebody brought
       with them an extraordinarily interesting seed, and then ...
       it grew, and there are many people now, so called cyborgs in
       the Media Lab and people working on wearable computers all over the
       place."
   </strong>
       <br/>
       -- -- Nicholas Negroponte, Founder, Director, and Chairman,
       MIT Media Lab
</blockquote>

<blockquote>
<strong>
"Steve Mann is the perfect example of someone... who persisted in his vision
and ended up founding a new discipline."
</strong>
<br/>
-- -- Nicholas Negroponte, Founder, Director, and Chairman, 1997
</blockquote>
<hr>

<h3>Background:</h3>
Since early childhood, as an amateur scientist and inventor, Mann has been
designing, building, and wearing computer systems, wearable vision systems,
and something he called ``Digital Eye Glass''.  He originally built it as a
seeing aid for electric arc
welding (i.e. as a smart welder's glass;
see "<a href=http://www.slashgear.com/quantigraphic-camera-promises-hdr-eyesight-from-father-of-ar-12246941/>Quantigraphic camera promises HDR eyesight from Father of AR</a>")
and later used it in his everyday life, being called
"the father of the wearable computer", February 8th, 2000 (IEEE ISSCC2000).

<h3>Founding of the MIT Wearable Computing Project:</h3>
In 1991, Mann was accepted into MIT (Massachusetts Institute of Technology)
and brought his Digital Eye Glass invention to the MIT Media Lab to found
the MIT Wearable Computing Project, as its first member.
Mann's portfolio (part of his application materials sent prior to being
accepted into MIT) included his
<a href=http://wearcam.org/swim/>S. W. I. M.
(Sequential Wave Imprinting Machine),
<br />
<img src="https://images.gitee.com/uploads/images/2021/1213/054356_1cefbf15_5631341.gif">
</a>
<br />
wearable augmented reality computer system
[Steve Mann, "Wavelets and Chirplets: Time-Frequency Perspectives",
pages: book cover + 99-128,
in "Advances in Machine Vision: Strategies and Applications",
Colin Archibald and Emil Petriu, editors, 1992].
<p>
At that time the Director of the MIT Media Laboratory was Nicholas Negroponte.
<p>
Broadcast nationally on CBC Television (``Steve Mann, Creator, Wearable Computing'', 1996), Negroponte, in his own words, said the following:
<blockquote>
<strong>
      "Steve Mann was, uh, building wearable computers in high school,
       and I think its [a] perfectly good example, that here's a young man
       that brought with him an idea... And when he arrived here a lot of
       people sort of said wow this is very interesting... I think it's
       probably one of the best examples we have of where somebody brought
       with them an extraordinarily interesting seed, and then it sort of,
       you know, it grew, and there are many people now, so called cyborgs in
       the Media Lab and people working on wearable computers all over the
       place."
</strong>
</blockquote>

<h3>See one of the following video file formats:</h3>
<ul>
 <li><a href=nn.mpg>MPEG download: http://wearcam.org/nn.mpg</a>
 <li><a href=http://vimeo.com/51330179>See it as part of a longer video
     of Steve Mann, selected art works, on Vimeo.com/51330179 (scroll forward past the first minute to get to the Negroponte interview)</a>
 <li><a href=http://www.glogger.mobi/v/75560>Streaming video from Glogger: http://www.glogger.mobi/v/75560</a>
</ul>

<hr>
<h3>Additional Negroponte quote:</h3>
<blockquote>
<strong>
<a href=http://wearcam.org/tpw_torontostar/>"Steve Mann is the perfect example of someone deemed to be on the lunatic fringe, but who persisted in his vision and ended up founding a new discipline."</a>
</strong>
<br/>(Bangor Daily News - Sep 26, 1997;
      the same quote later appeared in Toronto Star, 2001.)
</blockquote>
<br>
<img width=720 src="https://images.gitee.com/uploads/images/2021/1213/054505_73f1cec1_5631341.jpeg">

---

# [AR眼镜中的光学显示方案原理及其工艺全解析](http://vr.sina.com.cn/news/hz/2020-06-17/doc-iirczymk7483042.shtml)
2020-06-17 13:40:07   来源：新浪VR
### 1、AR眼镜中的光学显示方案

　　增强现实技术即AR技术是在展示真实场景的同时，通过图像、视频、3D模型等技术为用户提供虚拟信息，实现将虚拟信息与现实世界巧妙地相互融合，属于下一个信息技术的引爆点，据权威预测增强现实眼镜将会取代手机成为下一代的协作计算平台。以增强现实眼镜为代表的增强现实技术目前在各个行业开始兴起，尤其在安防和工业领域，增强现实技术体现了无与伦比的优势，大大改进了信息交互方式。目前比较成熟的增强现实技术中的光学显示方案主要分为棱镜方案、birdbath方案、自由曲面方案、离轴全息透镜方案和波导（Lightguide）方案。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190038_8cf86528_5631341.png "屏幕截图.png")
### 1.1 棱镜方案

　　棱镜方案以Google Glass为例，如图1中所示，其光学显示系统主要由投影仪和棱镜组成。投影仪把图像投射出来，然后棱镜将图像直接反射到人眼视网膜中，与现实图像相叠加。由于系统处于人眼上方，需要将眼睛聚焦到右上方才能看到图像信息，而且这一套系统，存在一个视场角vs体积的天然矛盾。Google Glass系统视场角较小，仅有15度的视场角，但是光学镜片却有10mm的厚度，而且亮度也不足，图像存在较大的畸变，所以产品进入市场后不久便被公司撤回。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190052_f25a1d4f_5631341.png "屏幕截图.png")
　　图1。 Google Glass眼镜产品实物图

### 1.2 Birdbath方案

　　Birdbath方案中的光学设计是把来自显示源的光线投射至45度角的分光镜上，分光镜具有反射和透射值（R/T），允许光线以R的百分比进行部分反射，而其余部分则以T值传输。同时具有R/T允许用户同时看到现实世界的物理对象，以及由显示器生成的数字影像。从分光镜反射回来的光线弹到合成器上。合成器一般为一个凹面镜，可以把光线重新导向眼睛。采用这种光学显示方案的AR头显装置主要有联想Mirage AR头显（图2（a））与ODG R8和R9（图2（b））。其中ODG有50度的视场角，其厚度则超过20mm。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190127_c7eae859_5631341.png "屏幕截图.png")
　　图2。 （a） Mirage头显装置；（b） ODG R9头显装置

### 1.3 自由曲面方案

　　自由曲面方案中一般采用有一定反射/透射（R/T）值的自由曲面反射镜，自由曲面是一种有别于球面或者非球面的复杂非常规面形，即用来描述镜头表面面形的数学表达式相对比较复杂，往往不具有旋转对称性。显示器发出的光线直接射至凹面镜/合成器，并且反射回眼内。显示源的理想位置居中，并与镜面平行。从技术上讲，理想位置是令显示源覆盖用户的眼睛，所以大多数设计都将显示器移至“轴外”，设置在额头上方。凹面镜上的离轴显示器存在畸变，需要在软件/显示器端进行修正。由于自由曲面不仅能为光学系统的设计提供更多的自由度，使系统的光学性能指标得到显著提高，而且为系统设计带来更加灵活的结构形式，因此成为近年来光学设计领域的研究热点，其中最具代表性公司有日本爱普生公司（如图3所示）以及美国梦境视觉公司的Meta系列（如图4所示）。日本爱普生公司的AR眼镜虽然在色彩、饱和度和成像质量方面博 彩，但是它仅有23度的视场角，而且有13mm的厚度。美国梦境视觉公司的Meta2系列AR眼镜虽然有90度的视场角，但是其厚度超过50mm，仅光机系统重量就约为420克。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190154_89b792f6_5631341.png "屏幕截图.png")
　　图3。 日本爱普生公司研发的AR眼镜。（a）产品实物；（b）成像光路。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190203_52445696_5631341.png "屏幕截图.png")
　　图4。 美国梦境视觉公司研发的AR头盔。（a）产品实物；（b）成像光路。

　　由上所述可知，棱镜方案、birdbath方案、自由曲面方案这三种方案中都存在一个不可规避的矛盾，即视场角越大，光学镜片就越厚，体积越大，也正是因为这一无可调和的矛盾限制了其在智能穿戴方面，即增强现实眼镜方面的应用。

### 1.4 全息透镜方案

　　全息透镜方案使用全息镜片独一无二的光学特性，其原理是将一个全息准直透镜（Hd）和一个简单的线性光栅（Hg）记录在同一个全息干板上，全息准直透镜将显示源射出的光束准直为平面波，并衍射进基底以进行全内反射传输，同时线光栅将光束衍射输出进入人眼。这种系统将全息光学元件作为耦合元件，结构紧凑的同时降低了对全息光学元件设计和加工的难度，同时降低了全息透镜的色散，而且具有大FOV和小体积的优势，因而迅速被人们所接受。但是受限于眼动范围比较小，而且由于全息透镜具有复杂的像差和严重的色散，因此用全息透镜成像效果并不理想。目前采用全息透镜方案的代表性厂家是North，如图5中所示即为North公司的基于全息透镜方案的AR眼镜产品实物图以及其成像光路示意图。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190219_c9d46c23_5631341.png "屏幕截图.png")
　　图5。 North公司研发的基于全息透镜方案的AR眼镜。（a）产品实物；（b）成像光路。

### 1.5 光波导方案

　　光波导方案在清晰度、可视角度、体积等方面均具优势，于是成为目前最佳的增强现实眼镜中光学显示方案，而且有望成为AR眼镜的主流光学显示解决方案。基于波导技术的AR眼镜，一般由显示模组、波导和耦合器三部分组成。显示模组发出的光线被入耦合器件耦入光波导中，在波导内以全反射的形式向前传播，到达出耦合器件时被耦合出光波导后进入人眼成像。由于用波导折叠了光路，一般系统体积相对较小。根据耦合器的原理，基于波导技术的AR眼镜，所使用的光波导技术总体上可分为几何波导方案和衍射光波导方案两种。

　　几何波导方案中一般包括锯齿结构波导和偏振薄膜阵列反射镜波导（简称偏振阵列波导）。其中主流的偏振阵列波导是通过利用多个等间距平行放置且有一定分光比的半透半反膜层来实现图像的输出和出瞳扩展，从而具有轻薄、较大的视场和眼动范围且色彩均匀的优势。衍射光波导方案主要有表面浮雕光栅波导方案和体全息光栅波导方案。浮雕光栅波导方案是采用纳米压印光刻技术制造，虽然具有大视场和大眼动范围的优势，但是也会带来视场均匀性和色彩均匀性的挑战，同时相关的微纳加工工艺也是巨大的挑战，生产成本较高。体全息光栅波导方案在色彩均匀性（无彩虹效应）和实现单片全彩波导上均具有优势，于是引起了AR光学模组生产产商的极大兴趣。

　　图6为波导方案的基本显示原理，耦入区域用于将微投影光机的光束耦入到波导片中，使得光束满足在波导片中全反射传播的条件，耦出区域用于将全反射传播的光束耦出波导片并传到人眼。耦入区域可以是反射镜、棱镜、浮雕光栅和体全息光栅等。耦出区域可以是阵列排布的半透半反射镜、浮雕光栅和体全息光栅等。本文将对几何光波导技术中的偏振阵列波导方案以及衍射光波导技术中的表面浮雕光栅波导方案和体全息光栅波导方案进行详细说明，并对表面浮雕光栅和体全息光栅的制备、加工工艺做出阐述，同时进一步地介绍谷东科技在该领域相关的研发情况。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190239_51ca1205_5631341.png "屏幕截图.png")
　　图6。 波导方案原理图

## 2、偏振阵列波导

### 2.1 偏振阵列波导原理

　　偏振阵列波导技术的波导镜片中通常采用有多个等间距平行放置且有一定分光比的半透半反膜层来实现图像的输出和出瞳扩展，该半透半反膜层具有角度选择性，且阵列排布。其工作原理示意图如图7中所示，图像源发出的光线经过目镜系统准直后，由波导反射面耦合进入波导，各视场光线依据全反射定理在波导中传播，光线入射到半透半反面上时，一部分反射出波导，另一部分透射继续传播。然后这部分前进的光又遇到另一个镜面，重复上述的“反射-透射”过程，直到镜面阵列里的最后一个镜面将剩下的全部光反射出波导进入人眼。由于波导可以具有多个半透半反面，每一个半透半反面形成一个出瞳，因此可以在基板厚度很薄的情况下，进行出瞳的扩展，实现大视场和大眼动范围显示。在经过多次反射后，便能将出射的光“调整”得比较均匀。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190302_8839d9c2_5631341.png "屏幕截图.png")
　　图7。 阵列光波导工作原理示意图

　　这项技术的扩瞳技术，设计较为复杂。设计时要充分考虑杂散光，人眼兼容性，各项性能指标。除此之外，均匀性也是最终用户体验的直观指标，如何控制多个膜层的反射和透过率，如何整机优化，如何控制镀膜工艺，才能保证整个眼动范围内的均匀性，也是研究的重点。为此谷东科技自主研发设计基于偏振阵列波导技术的光学模组，并在多次不断尝试总结后，得到了具有划时代意义的成果。

### 2.2 谷东科技-“七折叠、十二面体”超短焦AR光学模组M3010

　　近日，谷东科技发布了全新 “七折叠、十二面体”超短焦AR光学模组M3010（如图8），采用特殊选择的材料和工艺搭配，成功消除了同行产品固有的杂像、条纹感、鬼像、畸变、色散等疑难问题，在成像清晰度、最高亮度、色彩均匀度、重量、体积、功耗、漏光等方面皆突破了现阶段AR显示技术的极限，各项指标皆居于世界前列，真正集合了光波导模组极薄、极轻、极高的色彩还原等所有优势，并将其性能发挥到极致。图9中所示即为谷东科技公司近期推出的基于偏振阵列波导技术的光学模组M3010产品规格。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190316_3b368174_5631341.png "屏幕截图.png")
　　图8。 谷东科技-（a）七折叠光路；（b） 基于偏振阵列波导技术的光学模组M3010产品展示图

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190325_996ccc02_5631341.png "屏幕截图.png")
　　图9。 谷东科技-基于偏振阵列波导技术的光学模组M3010产品规格

　　谷东科技的“七折叠、十二面体”超短焦光学模组M3010具有以下超强性能。1、小：基于晶体材料的各向异性特性实现了光学器件的复用，将原本在光波导内朝一个方向传播的光线折叠成7段，使投影光机单元体积缩小85%；2、轻：重量大约为33克；3、透：波导镜片透光度超过普通建筑玻璃窗，可达85%以上；4、薄：折射畸变小于2mm；5、色：超高对比度、分辨度、色彩还原度与超全色域覆盖率，M3010标配LCOS作为像源，分辨率可达到1920*1080，提供接近人眼极限分辨能力的光学解析力，完全消除屏幕边界感，画质清晰细腻，图像反差锐利，不会有颗粒感。入眼最高亮度可达5000nit，色域覆盖率超过100%RGB，达到专业显示器水准，分毫毕现。6、零走光：得益于谷东科技独家研发的分光膜阵列波导片和光学结构，M3010模组在工作时不会出现漏光，更不会对外界暴露屏幕显示的内容，不论对隐蔽性要求极高的军用头盔还是消费娱乐的AR眼镜，该特性都至关重要；7、超视界：M3010采用top-down结构，水平视场完全无遮挡，整个视场尽收眼里，全视界，在提高用户体验的同时还解决了用户因佩戴眼镜遮挡视线造成的安全隐患的问题；8、低功耗：续航时间可达到10个小时左右；9、量产每年达10K片，量产良率和成本均达到世界一流水平；10、超严格的环境测试标准：面对极端高、低温环境，以及高湿度和持续盐雾侵袭，谷东科技的“七折叠、十二面体”超短焦AR光学模组M3010都能以远超行业平均水平的可靠性稳定工作；11、全面接受定制：AR眼镜“出圈”落地场景丰富，基于M3010的强大功能，各行各业的科技公司可在自己熟悉的领域定制广泛的智能化AR产品。我们坚信谷东科技的“七折叠、十二面体”超短焦光学模组M3010，必定能够拉开下一代显示技术革命的序幕，并为同样对技术抱持工匠精神的企业提供更优秀、更强大的AR产品以及优质的服务。

## 3、衍射光波导

### 3.1表面浮雕光栅波导

　　浮雕光栅波导方案即为使用浮雕光栅（SRG）代替传统的折反射光学器件（ROE）作为波导方案中耦入、耦出和出瞳扩展器件，其工作原理示意图如图10中所示。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190345_476ab7a6_5631341.png "屏幕截图.png")
　　图10。 衍射光波导和表面浮雕光栅的原理示意图

　　常用的浮雕光栅主要有一维光栅，其中包括倾斜光栅、梯形光栅、闪耀光栅和矩形光栅结构等，图11（a）中所示为倾斜光栅的扫描电镜（SEM）图。二维光栅主要为波导中常用的六边形分布的圆柱光栅结构，如图11（b）中所示为二维圆柱光栅结构的SEM图。以上光栅结构的特征尺寸均为纳米级。目前浮雕光栅波导方案最具代表的产品为微软的HoloLens系列12（a）以及WaveOptics公司的浮雕光栅波导系列产品12（b）。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190353_d20e052a_5631341.png "屏幕截图.png")
　　图11。 （a）倾斜光栅结构图； （b）二维圆柱光栅结构图

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190359_c084bf8e_5631341.png "屏幕截图.png")
　　图12。 （a） 微软的HoloLens2； （b） WaveOptics公司的浮雕光栅波导

　　3.2体全息光栅波导

　　体全息光栅波导方案采用体全息光栅作为波导的耦入和耦出器件，体全息光栅是一种具有周期结构的光学元件，它一般通过双光束全息曝光的方式，直接在微米级厚度感光聚合物薄膜内部干涉形成明暗分布的干涉条纹，从而引起了材料内部的折射率周期性变化。这个周期一般是纳米级的光栅结构，与可见光波长为一个量级，于是便可以对光线进行有效调制，通过对入射光发生衍射作用，从而改变光的传输方向。将体全息光栅和波导片结合，通过设计体全息光栅的相关参数（如材料折射率n、折射率调制因子和厚度等）可以调整体全息光栅的衍射效率。

　　体全息光栅波导技术的工作原理示意图如图13中所示，由微显示器产生的图像经过准直系统后变为平行光，平行光透过波导照射到入耦合端的全息光栅上，由于全息光栅的衍射效应使平行光传播方向改变，波导中的光线在满足全反射条件时，被限制在波导内沿波导方向向前无损传播。当平行光传播到出耦合端的全息光栅时，全反射条件被破坏，光线再次发生衍射变为平行光从波导中出射，进入人眼成像。当耦入的全息光栅与调制出射的全息光栅具有相同的周期结构，且镜像对称时，可有效消除色散。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190412_1aea6067_5631341.png "屏幕截图.png")
　　图13。 体全息光栅波导技术工作原理示意图

　　早期采用体全息光栅波导方案的代表性厂家为Sony和Digilens，随着该技术的日渐成熟，目前参与全息光栅衍射波导光学研究的公司数量不断增加，主要包括英国的TruLife和WaveOptics，以及美国的Akonia等。Sony出过一款高亮度的单绿色体全息光栅波导，如图14中所示，该结构采用双面体全息光栅作为入耦合端，达到了 85%的透射率，显示亮度为 1000cd/m2。但因体全息光栅的厚度较小，该系统效率较低，此外，仅能用于单色显示，现已停产。Digilens推出双层全彩体全息光栅波导，如图15中所示，该结构通过利用多个单色光栅实现彩色实现，可有效减少系统颜色的串扰，但该系统的效率不高，且因其双层波导结构，系统制造难度更大。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190419_475b92d1_5631341.png "屏幕截图.png")
　　图14。 Sony 公司双面体光栅结构全息波导。（a）产品实物；（b）成像光路。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190426_38aed233_5631341.png "屏幕截图.png")
　　图15。 Digilens全彩色体全息光栅波导。（a）产品实物；（b）成像光路。

　　谷东科技采用全息材料曝光方法做到将RGB三色合一到一片衍射波导上，利用相干记录，衍射复现的原理将图像传到人眼显示。主要有模拟设计，材料，以及工艺制备这三个方面。模拟设计需要自行编写复杂的计算模型；材料主要指HOE中的感光材料，对于全息光波导，需要其制造前后的低收缩比，高效率以及高均匀性；工艺方面，更多是需要全息技术的制造光路以及曝光经验，它和使用的材料非常相关。图16是谷东科技研发的单层全彩色体全息光栅波导相应的显示效果，视场角为30°。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190435_cfeb877d_5631341.png "屏幕截图.png")
　　图16。 谷东科技研发的单层全彩色体全息光栅波导的显示效果

　　4、衍射光波导的微纳制造

　　4.1 表面浮雕光栅波导的微纳制造

　　表面浮雕光栅从维度上可分为一维和二维光栅，而在结构上可分为直光栅、闪耀光栅和倾斜光栅。由于增强现实光波导用于可见光波段，为了实现较大的衍射效率和视场角，其特征尺寸一般在数百纳米，甚至几十纳米，且其性能对误差容忍度较小，所以对微纳加工制备提出了很大的挑战。目前的衍射光波导制备基本都是基于半导体制备工艺（如光刻、刻蚀工艺）完成。但是，由于这些方法受其复杂、昂贵的设备的限制，生产成本非常高，不适合光学模组的大批量制备。

　　图17中所示即为表面浮雕光栅模板制备或小批量制备工艺流程图，包括其扫描电镜图。对于直光栅，其工艺较为成熟，首先在基底上旋涂抗蚀剂层，通过干涉曝光或电子束曝光实现光栅的图案化，之后利用反应离子刻蚀（RIE）或电感耦合等离子体刻蚀（ICP）将图案转移到基底，并将抗蚀剂层去除，完成直光栅的制备。由于均匀性问题导致以HoloLens为代表的斜光栅光波导无法直接采用反应型刻蚀方案准备，所以制备工艺较为复杂，需要采用聚焦离子束（focused ion beam etching，FIBE）、离子束刻蚀（ion beam etching，IBE）、反应离子束刻蚀（reactive ion beam etching，RIBE）技术所制备。综合考虑到效率和均匀性，RIBE是其中较为合适的方案。首先，将基底上通过物理或化学方法镀一层硬掩模（如Cr）层，之后旋涂一层抗蚀剂层。同样利用干涉曝光或电子束曝光进行图案化，之后通过氯干刻蚀工艺将抗蚀剂图案转移到Cr层。在刻蚀工艺之后，用氧等离子体法剥离剩余的抗蚀剂层。接下来使用基于氟基的RIBE工艺用电离的氩离子束以倾斜的角度入射基底。在反应离子束刻蚀之后，通过标准的湿法刻蚀工艺去除Cr掩模，获得具有出色均匀性的斜光栅。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190445_652537aa_5631341.png "屏幕截图.png")
　　图17。表面浮雕光栅模板或小批量制备工艺流程

　　上述基于半导体工艺的制备成本昂贵，不适合光栅波导量产加工。因此，衍射光波导的复制工艺随即被开发出来以便实现大批量生产，而这种大规模的制造工艺依赖于高折射率的光学树脂，目前Magic Leap和WaveOptics已经进行了相关工艺的验证。复制工艺包括热压法（hot embossing）、紫外线纳米压印光刻法（UV-nano imprint lithography）和微接触压印法（micro contact printing，亦被称为软光刻）。其中紫外线纳米压印光刻是表面浮雕光栅波导批量生产中的常用方法。

　　具体工艺流程如图18所示，该工艺可分为两个阶段：纳米压印工作模具制备阶段和批量生产阶段。首先，通过上述模板制备工艺将图案加工到硅晶圆上以用作模板，通过纳米压印技术在更大的硅晶片上旋涂UV树脂并在上面印刷更多的模板。然后使用紫外线对印刷的结构进行曝光以固定树脂。最后通过重复上述过程批量生产多图案的压印模具。在批量生产的过程中，使用多图案的模具来生产表面浮雕光栅波导，然后使用功能性涂层覆盖波导，并用激光切割技术分离，最后将不同结构的波导堆叠实现光学模组的制备。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190452_f2cc02c2_5631341.png "屏幕截图.png")
　　图18。表面浮雕光栅大批量复制量产工艺流程

　　4.2 体全息光栅波导的微纳制造

　　体全息波导关键元件是体全息光栅，体全息光栅的制备正是利用了全息技术的特性，通过激光激发的两个有一定夹角的平面光波相互干涉，并将干涉图案曝光附着在基底上的光敏材料上形成干涉条纹来获得的，材料特性根据光的强度分布而变化，最后获得具有折射率周期性变化特性的材料。制备体全息波导的材料包括卤化银、重铬酸盐明胶、光敏聚合物、全息高分子分散型液晶以及其他更奇特的材料。

　　全息技术是一种利用光学相干原理来记录和获取物光波的振幅和相位信息的方法。其利用干涉记录、衍射再现的原理，把具有振幅和相位信息的物光波与参光波相干涉产生的干涉条纹以强度分布的形式记录成全息图，从而把物光波的全部振幅和相位信息记录在感光材料上。全息是一种主动式相干成像技术，全息的记录光路（如图19（a）中所示）主要完成两个方面的功能，一是完成被测物体的相干照明，通过物体的透射或反射形成物光波；二是利用参考光波与物光波发生干涉，形成全息图。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190502_434f97df_5631341.png "屏幕截图.png")
　　其中，T0表示零级衍射光，对应于参考光波的透射光波；T+1表示 +1级衍射光，携带了原始物光波的信息；T-1为 -1级衍射光，携带了物光波的共轭信息。在光学全息中， +1级衍射光能够形成物体的虚像，可以用眼睛直接观察，而 -1级衍射光能够形成物体的实像，可以利用屏幕接收。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190511_b380b7f7_5631341.png "屏幕截图.png")
　　图19。 光学全息的记录与再现过程示意图

　　理想全息光栅的衍射级次只有0级和±1级，全息光波导显示利用的是0级光在光波导内不断地全反射，而-1级光不断地从波导表面出射。光栅衍射的几何示意图，如图20中所示。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190518_bdd15535_5631341.png "屏幕截图.png")
　　图20。 全息光栅衍射几何示意图

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190527_d422959a_5631341.png "屏幕截图.png")
　　由上述三式可以得出对于特定波长、波导介质以及光线入射角度，满足全反射条件的光栅周期应满足一定条件。

　　全息光栅按照其结构可以分为透射型和反射型全息光栅，两者根本区别在于记录方式不同，即两束记录光的传播方向不同，从而造成记录材料内部干涉条纹面的不同取向。透射型全息光栅在记录时，物光和参考光是从记录介质的同侧进行入射，而反射型全息光栅在记录时，物光和参考光则是从记录介质的两侧相向入射。

　　全息光栅根据记录介质的厚度与干涉条纹间距的相对厚度关系又可以分为面全息光栅和体全息光栅。面全息光栅与体全息光栅的评判标准用Q值来表征，当Q≥10时为体全息光栅，反之为面全息光栅。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190535_27d038d5_5631341.png "屏幕截图.png")
　　体全息光栅的微结构在体光栅的内部，所以其衍射主要是材料的体效应。当入射光满足布拉格条件时，体全息光栅会有极高的衍射效率，而如果偏离了布拉格条件，衍射效率则会迅速下降，这个特性使体全息光栅具有明显的角度和波长选择性。当用做耦合器件时，体全息光栅可以把波导中，具有特定波长和角度的光从波导中耦合出来，却又不会遮挡外界真实场景的视野，因此是一种理想的耦合器件。

　　上述体全息光栅的制备过程仅适用于小批量验证，而对于大批量生产，则需要开发更加经济的方案，以Sony和DigiLens为代表的公司开发了体全息波导的加工工艺流程。如图 21 中展示了制备体全息波导的卷对卷（roll-to-roll）工艺。首先，使用双束干涉曝光法在附着在卷胶上的光敏聚合物膜内形成体全息波导；第二步，通过注射成型法形成高质量的环烯烃聚合物塑料波导。为了获得合格的图像，波导的翘曲必须小于5um，并且有效区域的厚度变化应小于1um。然后进行全息光学元件的转移工艺以将全息波导膜准确地与塑料波导对准粘贴；之后将塑料全息波导进行切割；最后在配色过程中，将红、蓝塑料波导与绿色塑料波导对准并用UV树脂将其封装固定。塑料基底在每次加工之前和之后都均应保持平坦是冲压和配色过程中都面临的挑战。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1126/190543_0a9aa742_5631341.png "屏幕截图.png")
　　图21。 卷对卷体全息波导制备工艺

　　5、展望

　　光学穿透式头戴显示（AR眼镜）作为AR技术的主要硬件载体，近年来，受到了科学界和产业界的广泛关注，所以本文对当前AR眼镜的光学显示方案，棱镜方案、birdbath方案、自由曲面方案、离轴全息透镜方案以及光波导（Lightguide）方案都做了阐述。尤其是光波导方案作为现在AR眼镜的主流光学显示方案，文中采用了大量章节对几何光波导方案中偏振阵列光波导方案，以及衍射光波导方案中的表面浮雕光栅波导方案和体全息光栅波导方案做了详细介绍，并展示了谷东科技的部分相关样品。

　　偏振阵列波导方案具有轻薄、大眼动范围和色彩均匀性好的优点，但是由于偏振阵列波导本身工艺的复杂，在设计和加工均有很高的技术壁垒，如分光膜阵列镀膜工艺不够完善，分光面表面平整性、精度要求高，难以降低成本等。谷东科技在此领域深耕多年，完全实现了从设计到加工的自主化，率先在国内实现了偏振阵列波导的大规模量产。谷东科技从研发阶段开始就将工艺进行拆分细化，建立了一套保密可控的量产供应链体系，年产能已达10K片，真正实现了量产，也代表了偏振阵列波导技术新的发展里程碑。成熟的设计方案和大规模的量产能力使得偏振阵列波导方案在未来五年内都将是AR领域的主流方案。比如谷东科技的“七折叠、十二面体” 超短焦AR光学模组M3010系列产品，采用谷东科技自主研发的工艺流程方法，通过长时间的工艺实验制定了严格制程管控和测试标准，该光学模组的量产良率和成本均达到世界顶尖水平。

　　体全息光栅波导方案利用全息光栅来作为光线的耦入/耦出装置，其将波导的全反射特性和全息光栅的衍射特性相结合，可实现大视场、大出瞳图像输出，从而被应用于新一代头盔显示系统中，而且其具有整体质量和体积更为紧凑的优点。相对于传统的阵列光波导中采用几何光学元件来作为光线的耦入/耦出装置，全息光波导可有效降低显示系统的厚度和重量。体全息光栅波导还具有色彩均匀性好和易于实现单片彩色波导的优势，此外，体全息光栅作为一种通过光学曝光得到的体全息光学元件，有着集成其他光学器件功能的可能性，比如可以将显示模组中的准直镜集成进来，使入耦合器同时具有耦合和准直的功能。但是其采用全息干涉曝光的方法进行波导片的加工，限制了其大规模的量产。同时，做大FOV需要叠加多层全息光栅，增加了工艺难度，做彩色波导片需要高密度的曝光材料，进一步增加了工艺难度。虽然体全息光栅波导技术还面临着各种难题，设计方案的进一步成熟和量产良率的提升预计还需要一定的时间，但是谷东科技公司积极开展部署体全息光栅波导设计研发以及制备，在设计与加工方面推动体全息光栅波导方案的发展。

　　综上所述，偏振阵列波导方案和体全息光栅波导方案是目前两个最有前景的主流AR方案，一个代表着现在，一个代表着未来。谷东科技率先在国内实现偏振阵列波导的大规模量产，同时积极部署体全息光栅波导方案，希望为AR事业的发展尽力发光。

新浪声明：[新浪网登载此文出于传递更多信息之目的，并不意味着赞同其观点或证实其描述。](https://images.gitee.com/uploads/images/2021/1126/190554_789a6bfe_5631341.png)