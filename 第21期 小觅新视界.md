<p><img src="https://images.gitee.com/uploads/images/2022/0117/235145_51e6635c_5631341.png" title="vslam2.png" height="999px"></p>

「小觅新视界」
- vSLAM-AI行业中的百万年薪专业
- 视觉惯导里程计时差的在线标定
- 3D相机TOF、机器视觉之协方差矩阵 
- Astar.ai 让SLAM变得微不足道

> 神奇的日子 5.20 确定的作业标题，这是 @庞琳勇 博士 发布《你想进入AI行业中最热门的专业拿到百万年薪吗？》一段时间后的小收获，他的北京伙伴接到了我的邀请，这个鸽了大半年的专题就是从那个时候开始的。但 SLAM 这项技术则要早一些，FlameAR 一直伴随着技术的发展不断逼近目标，AR 技术中有两个 SLAM 方案分别是室内和室外，通过手机摄像头为 AR 动态构建 3D 画布。所以一看到 庞博士的宣传片没几帧，我就已经跃跃欲试了，感觉他能帮我实现目标。带着这份期待，我发出了邀请信 ... 

> 带着歉意，也带着喜悦，昨天请来了 实事求视 的寄语，因为作业即将完成，目标也逐渐清晰地展现在了脑海中，于是我选择了3和1这两个数字，并体现在了本期封面的设计上，从素材堆中，我选择了最有代表性的三幅图，右下角是深度相机和普通相机还有实景的比对，这和庞博的 VSLAM 视频的封面同源，没有比之前更清晰地知道了深度是怎么表示的。左侧的场景构建图的颗粒感是我最喜欢的，如油画一样鲜艳夺目。右上则选择了庞博的伙伴 ASTAR 的首页图，“让 SLAM 变得无关紧要” 与主题图的惬意生活融为一体。道路的尽头我选择了以为数字艺术家的国际象棋渲染图，因为，这是火焰棋实验室的缘，也是本期目标，故事篇尾再叙... 而方格矩阵的灵感来自网络海报，一个方格就是一个像素，这个构建虚拟世界的元素，就是一个不灭的小宇宙 :pray:

# <a href="https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8315741_link" title="你想进入AI行业中最热门的专业拿到百万年薪吗？" target="_blank"><img src="https://images.gitee.com/uploads/images/2022/0115/172506_8073fda9_5631341.png" width="169px"> 《[小觅新视界](https://gitee.com/yuandj/siger/issues/I3Y7Y0)》

- 你想进入AI行业中最热门的专业拿到百万年薪吗？
- 你在为选哪一个视觉传感器才能快速验证算法而烦恼吗？
- 你在为复现论文时总是没有办法达到他的State of the Art的效果而冥思苦想吗？
- 的你在为工作中开源VSLAM的精度稳定性总是让人大失所望而苦恼吗？

今天我们携 MYNTeye 小觅双目精心打造了一套VSLAM的高阶课程，为你进军百万年薪铺平道路。去年华为天才人才计划拿到最高档200万年薪的两个毕业生之一是香港科技大机器人专业的秦通，他博士研究的方向是机器视觉 VSLAM 视觉惯导融合，多传感器定位。主要是使用我们的 MYNTeye 小觅双目。

> 小觅双目是全球第一个多传感器融合的三维视觉加惯导的主动双目模组，又被特斯拉称为虚拟激光雷达技术，在全球有3000多家客户使用，几乎涵盖所有全球知名的机器人、人工智能和无人驾驶公司，包括大家熟知的波士顿动力（过剩动力）、达芬奇手术机器人等。

之后有很多同学来信，希望我们能开设一门三维视觉和 VSLAM 的实战课程。我们在准备了一年之后，现在来了。这个课程由 StanfordRobotics.AI 斯坦福机器人的两位三维视觉和 VSLAM 的世界顶级专家博士亲自授课。

- **庞琳勇 Leo 博士** :
  - 斯坦福大学机械工程博士和计算机硕士双学位。
  - 中科大北硕是从光测泰斗伍小平院士，并提前一年毕业。
  - 庞博士是机器人三维视觉双目和半导体的专家，有22年硅谷和国内创业，纳斯达克上市，公司行政管理、市场和营销的经验，其中一家创业公司在纳斯达克上市，主导了其上家创业公司的收购。
  - 庞博士是小觅智能，MYNT AI和史丹福机器人的创始人，拥有38项专利，还有28项待批，83篇论文在国际期刊和学术会议上发表。

- **张国铉 Alan 博士** :
  - 韩国汉阳大学 VSLAM 博士。
  - 美国普林斯顿大学、新加坡理工大学、美国 Drexel 大学研究 VSLAM 的博士后。
  - 张博士是前美国 Rainbow 机器人公司的 VSLAM 首席科学家。
    > Rainbow 的机器人 Hubo 在2015年美国 DARPA 举办的全球 机器人挑战赛中击败了 波士顿动力和 MIT，斯坦福，和 CMU等名校的机器人，获得了冠军。
  - 张博士曾在硅谷创立 Astar.AI，研发出了全球领先的基于物体边缘特征的鱼眼双目 VSLAM 系统。其核心专利 VSLAM system 已经获得美国专利局的授权。

这门 VSLAM 高级进阶课程总共20节课，内容涵盖以下 10 个方面:

1. 三维视觉的历史起源和演变，
2. 三维视觉传感器的三大技术路线和应用场景。
3. VSLAM 的基本原理，
4. VSLAM 和多传感器融合，VIO
5. 目前主流的 VSLAM 开源框架及其优缺点
6. MYNTeye 小觅双目，从零上手。
7. 用 MYNTeye 小觅双目实现 VIO，
8. 用 MYNTeye 小觅双目实现 VSLAM
9. 如何解决 VSLAM 在实际应用中的精度、长时间运行和地图加载等核心问题。
10. 如何在开源方案上构建商用级的 VSLAM。

 **课程收获** 

1. 首先，你会拿到一个商用级基础版的 VSLAM 源码，
2. 其次，是由张国铉博士亲自批改作业，
3. 答疑时间在线免费解答会员的 VSLAM 相关技术疑问，指导学生的工作，
4. 可选张国铉博士 VSLAM 的工程技术咨询服务（这个是￥2000元人民币/每小时）

所有完成课程的学生将获得美国 StanfordRobotics.AI 颁发的证书，成绩优秀者将被推荐到中国和美国硅谷的各大机器视觉、机器人和无人驾驶公司。成绩特别优秀，并申请美国和其他海外大学同学，还可以得到价值 ￥8.8万人民币的庞博士的推荐信。

 **课程费用：￥12800人民币** 包括：课件、小觅双目双目模组。
（5月15号之前报名注册，优惠价￥9800人民币。）

 **要求：** 研究生毕业或者在读，有一定的数理基础，有基本的编程能力，有阅读英文文献的能力。

这次机会难得，名额有限，要报名就赶紧行动吧。  
国内报名咨询：王齐（手机和微信，139 10124776）

- face

  <p><img src="https://images.gitee.com/uploads/images/2022/0116/230026_aee286c3_5631341.png" height="99px" title="2022-01-16-224854_507x587_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/230057_514e7340_5631341.png" height="99px" title="2022-01-16-224912_507x587_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/230113_3585e7ff_5631341.png" height="99px" title="2022-01-16-224929_505x585_scrot.png"></p>

- State of art

  <p><img src="https://images.gitee.com/uploads/images/2022/0116/230145_ea7e45e3_5631341.png" height="99px" title="2022-01-16-225039_506x581_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/230235_1711850b_5631341.png" height="99px" title="2022-01-16-225053_507x586_scrot.png"></p>

- 飞控PILOT

  <p><img src="https://images.gitee.com/uploads/images/2022/0116/230302_19b88de7_5631341.png" height="99px" title="2022-01-16-225130_506x594_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/230318_f2eb7b07_5631341.png" height="99px" title="2022-01-16-225157_502x589_scrot.png"></p>

- minteye 小觅双目 秦通 惯导

  <p><img src="https://images.gitee.com/uploads/images/2022/0116/230351_3e599df1_5631341.png" height="99px" title="2022-01-16-225210_503x586_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/230433_d30464c0_5631341.png" height="99px" title="2022-01-16-225239_507x585_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/230455_e9ac8706_5631341.png" height="99px" title="2022-01-16-225301_497x576_scrot.png"></p>

- XILINX

  <p><img src="https://images.gitee.com/uploads/images/2022/0116/230512_ea989689_5631341.png" height="99px" title="2022-01-16-225336_507x586_scrot.png"></p>

- darpa机器人2015

  <p><img src="https://images.gitee.com/uploads/images/2022/0116/230536_c5d85b56_5631341.png" height="99px" title="2022-01-16-225425_503x591_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/230612_f92fd2e0_5631341.png" height="99px" title="2022-01-16-225439_505x589_scrot.png"></p>

- AStar.ai 鱼眼视觉,专利

  <p><img src="https://images.gitee.com/uploads/images/2022/0116/230700_0b001dfa_5631341.png" height="99px" title="2022-01-16-225501_494x481_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/230730_55e1e855_5631341.png" height="99px" title="2022-01-16-225522_507x593_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/230806_e371e212_5631341.png" height="99px" title="2022-01-16-225540_506x584_scrot.png"></p>

- VSLAM 高阶课程，多传感器融合，VIO，用上小觅的VIO，VSLAM，

  <p><img src="https://images.gitee.com/uploads/images/2022/0116/230827_5b6ee74a_5631341.png" height="99px" title="2022-01-16-225556_504x439_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/230900_c10e6e30_5631341.png" height="99px" title="2022-01-16-225623_509x419_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/230936_72446fc3_5631341.png" height="99px" title="2022-01-16-225640_510x444_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/231030_96f278f7_5631341.png" height="99px" title="2022-01-16-225652_515x432_scrot.png"></p>

- 3DMAPPING&OVERVIEWING，商业精度，启动定位。

  <p><img src="https://images.gitee.com/uploads/images/2022/0116/231144_fdd6fd4f_5631341.png" height="99px" title="2022-01-16-225708_524x445_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/231223_e069e544_5631341.png" height="99px" title="2022-01-16-225723_509x418_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/231237_3c6df8d0_5631341.png" height="99px" title="2022-01-16-225750_513x290_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/231306_f907844f_5631341.png" height="99px" title="2022-01-16-225804_506x303_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/231320_34fe19a8_5631341.png" height="99px" title="2022-01-16-225827_490x293_scrot.png"></p>

- 王齐助教

  <p><img src="https://images.gitee.com/uploads/images/2022/0116/231359_cd86466f_5631341.png" width="99px" title="2022-01-16-225857_510x478_scrot.png"></p>

# [【机器视觉】史丹福-天使轮商业计划书 BP V4.1.pdf](https://max.book118.com/html/2021/0721/7130164124003146.shtm)

bing `StanfordRobotics.AI`

<p><img src="https://images.gitee.com/uploads/images/2022/0116/021919_49730048_5631341.png" height="99px" title="2022-01-16-021428_1200x648_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/021940_93469a70_5631341.png" height="99px" title="2022-01-16-021454_1199x671_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/021958_236cf433_5631341.png" height="99px" title="2022-01-16-021516_1200x668_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022036_23427518_5631341.png" height="99px" title="2022-01-16-021536_1199x672_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022053_054b9beb_5631341.png" height="99px" title="2022-01-16-021550_1200x673_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022113_1be1c07d_5631341.png" height="99px" title="2022-01-16-021606_1198x674_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022133_0bca769a_5631341.png" height="99px" title="2022-01-16-021622_1197x672_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022154_af758a5f_5631341.png" height="99px" title="2022-01-16-021636_1199x671_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022214_6c798172_5631341.png" height="99px" title="2022-01-16-021650_1199x675_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022235_76ce2a28_5631341.png" height="99px" title="2022-01-16-021703_1199x671_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022258_e9f33e44_5631341.png" height="99px" title="2022-01-16-021716_1198x672_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022325_b4656890_5631341.png" height="99px" title="2022-01-16-021728_1196x673_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022401_85787759_5631341.png" height="99px" title="2022-01-16-021740_1198x670_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022421_0bef2473_5631341.png" height="99px" title="2022-01-16-021753_1197x672_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022437_178f8a4d_5631341.png" height="99px" title="2022-01-16-021804_1199x669_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022457_62d25224_5631341.png" height="99px" title="2022-01-16-021818_1200x673_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/022516_2fd5f362_5631341.png" height="99px" title="2022-01-16-021831_1201x675_scrot.png"></p>

# [视觉惯导里程计时差的在线标定-秦通](https://pan.baidu.com/s/1BQW7UNJimpo50fDVZHZJhg) 密码:k4z8

<p><img src="https://images.gitee.com/uploads/images/2022/0116/130705_e2490ce9_5631341.png" height="99px" title="2022-01-16-130127_792x447_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/130728_7928ad15_5631341.png" height="99px" title="2022-01-16-130139_793x448_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/130747_291a6210_5631341.png" height="99px" title="2022-01-16-130149_797x450_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/130804_9e04dc1e_5631341.png" height="99px" title="2022-01-16-130200_798x449_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/130818_221cdaa7_5631341.png" height="99px" title="2022-01-16-130212_794x449_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/130833_4170e4f4_5631341.png" height="99px" title="2022-01-16-130212_794x449_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/130909_97e430fc_5631341.png" height="99px" title="2022-01-16-130231_796x447_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/130935_71152d0b_5631341.png" height="99px" title="2022-01-16-130243_792x451_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/131001_71267772_5631341.png" height="99px" title="2022-01-16-130253_794x448_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/131013_81791e05_5631341.png" height="99px" title="2022-01-16-130315_793x449_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/131029_b4667b84_5631341.png" height="99px" title="2022-01-16-130326_796x448_scrot.png"> <img src="https://images.gitee.com/uploads/images/2022/0116/131041_d7f2d6a2_5631341.png" height="99px" title="2022-01-16-130338_790x446_scrot.png"></p>

秦通，本科浙江大学，博士香港科技大学。出生於1993年的秦通，2015年获得浙江大学控制系学士学位，此后进入香港科技大学机器人研究所攻读博士，师从沈劭劼教授。其研究主要包括机器视觉SLAM，视觉惯导融合，多传感器定位。在IEEE TRO、ICRA、IROS、ECCV等国际顶级期刊和会议发表多篇论文，获得IEEE IROS 2018最佳学生论文奖。

秦通曾是浙江大学ZMART队2014年的队长。这支队伍用了5年的时间夺下国际空中机器人大赛的世界冠军。2014年9月，大四开学的第一天，秦通就已被香港科技大学录取硕博连读，提前预定好进入华为之前5年的人生。

在攻读博士期间，秦通已经成为业界名人，2018年10月，秦通在网上开了公开课，专门讲“视觉惯导里程计时差的在线标定”。

> [引自：天才“吸铁石”华为新添俄罗斯00后编程冠军](https://gitee.com/yuandj/siger/issues/I4QR64#note_8319848_link)

# [3D相机（二）--TOF相机](https://zhuanlan.zhihu.com/p/85519428)

n次方，工业机器人视觉定位及视觉检测

 **文章结构** 
- 前言
- TOF相机工作原理
- 工业领域应用
- 一些TOF研究机构

### 1.前言
前面介绍过三维重建有两个基本原则:三角测量和飞行时间(TOF)测量。三角测量使用的基本数学定理是，一旦知道三角形前两点的坐标和连接前两点到第三点的射线的两个角，就可以计算三角形第三点的坐标。相比之下，TOF相机发射辐射并测量辐射反射回传感器的时间。

飞行时间(TOF)照相机发射辐射并测量辐射反射回传感器的时间。目前有两种技术可以实现：PM（脉冲调制） TOF和CWM（连续波模拟） TOF。PM TOF相机发出辐射脉冲，并直接或间接测量辐射脉冲到达场景中的物体并返回相机所需的时间。而CWM TOF相机则发射振幅模拟辐射，测量发射和接收辐射之间的相位差。

CWM TOF相机则发射振幅辐射，测量发射和接收辐射之间的相位差。在过去的很多年里，主导的设计是CWM TOF相机，但是Foix等人讨论的许多CWM TOF摄像机并没有流行起来，甚至不再在商业上可用。看起来PM TOF相机似乎会成为工业3D相机的趋势。

### 2.TOF相机工作原理
 **CWM飞行时间相机** 

CWM TOF相机发射辐射，通常在近红外范围，即振幅调制为一定频率fm的正弦波，在15到30兆赫之间，辐射被场景中的物体反射并被相机接收，相机在一个调制辐射的周期内测量四次反射辐射的量，这将创建测量mi, i = 0, … , 3, 间隔的时间间隔为π/ 2的接收波。发射波和接收波之间的相移可以解调如下

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/183855_a32bc4dd_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/183903_881707ed_5631341.png "屏幕截图.png") 表示双参数的反正切函数,它返回的结果是[0,2π)。此外，这四个测量值可以用来计算场景强度

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/183911_0c2f14f6_5631341.png "屏幕截图.png")

和幅值

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/183919_070f65a6_5631341.png "屏幕截图.png")

可以用来预测测量的质量。在传感器的集成时间(曝光时间)内进行多次测量，以提高信噪比。

该相位在所谓的无歧义距离范围内是唯一的：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/183927_0751c130_5631341.png "屏幕截图.png")

其中 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/183938_7f90d36b_5631341.png "屏幕截图.png") 是光速，举个例子， ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/183949_97920d0e_5631341.png "屏幕截图.png") ,则 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/183957_e9baaf5d_5631341.png "屏幕截图.png") . 如果场景中存在距离大于 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184013_ef198238_5631341.png "屏幕截图.png") 的物体，则相位将出现折回干扰。这样一来，必须使用相位展开算法，这是非常耗时的，几乎从未使用过。因此，对于距离大于 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184217_290df4a5_5631341.png "屏幕截图.png") 的对象，将返回一个错误的对象距离。

如果对象距离d在无歧义范围内，则可计算为

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184229_3ab2e5c7_5631341.png "屏幕截图.png")

CWM TOF相机会发生系统和随机的错误。例如,距离测量可能依赖于积分时间,这可能会导致错误1到10厘米,相机内部的散射辐射会导致相对较大的距离误差,因为距离的测量可能取决于场景中的物体的反射,故可能存在系统的深度失真(摆动)，因为发射的辐射并不完全是正弦波。

CWM TOF相机可以有高达30HZ的高帧率。然而，它们的缺点之一是相对较小的分辨率:大多数商用传感器的分辨率低于320X240像素。

 **PM 飞行时间相机** 

PM TOF相机发射辐射脉冲，通常在近红外范围内，直接或间接测量辐射脉冲从辐射源到目标再返回传感器的飞行时间。让这个往返时间称为 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184252_fd6dd1c2_5631341.png "屏幕截图.png") 。然后，相机到物体的距离为

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184301_1bbe7684_5631341.png "屏幕截图.png")

虽然 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184314_fba56ea2_5631341.png "屏幕截图.png") 的直接测量在原则上是可行的，但在PM TOF相机中，往返时间是通过测量传感器接收到的辐射强度间接推断出来的。

一种基于 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184329_0d92bcde_5631341.png "屏幕截图.png") 间接测量的PM TOF相机，其工作原理是发射一定持续时间 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184341_0de4d6e0_5631341.png "屏幕截图.png") 的辐射脉冲，如 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184353_f30ba471_5631341.png "屏幕截图.png") 。脉冲持续时间决定了可以测量的最大目标距离(距离范围)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184402_d4f7e25e_5631341.png "屏幕截图.png")

例如，对于 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184412_359dffe1_5631341.png "屏幕截图.png") ，最大距离为4.497 m。在三个积分周期内测量返回到传感器的辐射。第一积分周期与辐射脉冲的发射同步。在三个积分周期内测量返回到传感器的辐射。第二个集成周期紧跟着第一个集成周期。这两个积分周期测量的是场景中物体反射的相机辐射强度。这两个测量值是用来推断场景中物体距离的主要数据。第三个积分周期发生在辐射脉冲发射前或发射后足够长的时间。其目的是对场景中物体反射的环境辐射效应的距离测量进行校正。

我们检查前两个积分周期，并假设场景中没有环境辐射反射。在第一个集成期间，靠近相机的物体反射的辐射会对传感器上的电荷产生很大的贡献。物体离传感器越近，相机上的电荷就越高。另一方面，在第二次积分期间，距离相机较远的物体反射的辐射对传感器的电荷有很大的贡献。物体离传感器越远，相机上的电荷就越高。例如，在d = 0处的物体在第一个积分周期内只产生一个电荷，而在第二个积分周期内由于在第一个集成期间，整个辐射脉冲返回到传感器而不产生电荷。另一方面，d = dmax处的物体在第二次积分期间只会产生一个电荷，因为在第一次积分期间脉冲无法返回传感器。最后一个例子是，在d = dmax/2处的物体在第一个和第二个积分期间会产生相等的电荷。这说明td的飞行时间可以从传感器上的电荷推断出来。让前两个积分周期的电荷分别用q1和q2表示。然后，可以证明

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184440_6421936b_5631341.png "屏幕截图.png")

注意，对于距离大于dmax的对象，在本例中，q1 = 0，因此降为td = tp。这就是为什么距离大于dmax的物体无法被正确测量的原因。

上式假设没有环境辐射。这在实践中很少是真的。第三个积分周期可用于校正环境辐射效应的深度测量。

让第三个积分周期产生的电荷记为q3。然后，q3可以从q1和q2中减去，抵消环境辐射，得到

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184456_2d43c1d5_5631341.png "屏幕截图.png")

因此，场景中某点的距离

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184505_06f7d50b_5631341.png "屏幕截图.png")



上述测量在传感器的曝光时间内进行多次，以提高信噪比。

目前还不存在对PMTOF相机的系统和随机错误影响的全面分析。我们可以假设已知的对CWM TOF相机存在影响的因素也会影响PMTOF相机。例如，我们知道PM TOF相机会受到依赖于场景中物体反射率的深度偏差的影响。PM TOF相机的时间噪声可以达到几厘米级。

PM TOF相机支持高帧率(高达30赫兹)。此外，它们提供比CWM TOF相机更高的分辨率。目前市面上出售的PM TOF相机的分辨率从320×240到1280×1024像素不等。

### 3. 工业领域应用
与立体相机或三角测量系统比，TOF相机体积小巧，跟一般相机大小相去无几，非常适合于一些需要轻便、小体积相机的场合。TOF相机能够实时快速的计算深度信息，达到几十到100fps。而双目立体相机需要用到复杂的相关性算法，处理速度较慢。TOF的深度计算不受物体表面灰度和特征影响，可以非常准确的进行三维探测。而双目立体相机则需要目标具有良好的特征变化，否则会无法进行深度计算。TOF的深度计算精度不随距离改变而变化，基本能稳定在cm级，这对于一些大范围运动的应用场合非常有意义。这些优势都使得它更易在工业上的推广。

TOF 相机目前的主要应用领域包括：

物流行业：通过 TOF 相机迅速获得包裹的抛重（即体积），来优化装箱和进行运费评估；

安防和监控：

进行 Peoplecounting 确定进入人数不超过上限；

通过对人流或复杂交通系统的counting，实现对安防系统的统计分析设计；

敏感地区的检测对象监视；

机器视觉：工业定位、工业引导和体积预估；

替代工位上占用大量空间的、基于红外光进行安全生产控制的设备;



### 4. 一些TOF研究机构

<1> Dynamic 3D Vision (2006-2010):

研究领域：多芯片2D/3D传感器, 动态场景重建, 目标位置识别和光场计算

- Welcome to PMD
[​www.zess.uni-siegen.de/pmd-home/dyn3d](https://www.zess.uni-siegen.de/pmd-home/dyn3d)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/184553_a6e930a0_5631341.png "屏幕截图.png")

<2>ARTTS (2007-2010):

全称：“Action Recognition and Tracking based on Time-of-Flight Sensors”

- ARTTS
[​www.artts.eu/](https://www.artts.eu/)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/185217_fc9ef667_5631341.png "屏幕截图.png")

研究领域：开发更加小型更加便宜的新一代TOF相机；将HDTV与TOF技术相结合(iii) 基于动作跟踪和识别算法的多模式接口和交互系统


<3>Lynkeus (2006-2009):

- www.lynkeus-3d.de
[​www.lynkeus-3d.de/](https://www.lynkeus-3d.de/)
研究领域：致力于工业应用领域的高分辨率和鲁棒性的TOF传感器，例如自动化和机器人导航

<4>3D4YOU(2008-2010):

- www.3d4you.eu
[​www.3d4you.eu/](https://www.3d4you.eu/)
研究领域: 构建3D-TV产品线,从3电影中实时获取点云数据并且转换为3D显示到家庭电视中3D4YOU 应用ToF range cameras初始化估计多个高清晰度相机的深度以及初始化计算3D场景图像的深度。

<5>MOSES(2008-2012):

全称：“Multi-Modal Sensor Systems for Environmental Ex-ploration (MOSES)”

研究领域：基于传感器的多方面的应用，包括基于TOF的人机接口和多传感器融合

- www.zess.uni-siegen.de/ipp_home/moses
[​www.zess.uni-siegen.de/ipp_home/moses](https://www.zess.uni-siegen.de/ipp_home/moses)

编辑于 2019-10-14 17:37

# [机器视觉绕不过的数学问题--协方差矩阵](https://zhuanlan.zhihu.com/p/86624275)

n次方，工业机器人视觉定位及视觉检测

今天看论文的时候又看到了协方差矩阵这个东西，作图像基本就绕不过它，虽然公式网上都有，但是不太明白它的物理意义，通俗的说，它到底是干嘛的，为啥总露脸，哪里都看得到~~单以前看PCA的时候就特困扰，没想到现在还是搞不清楚，索性开始查协方差矩阵的资料，恶补之后决定马上记录下来，嘿嘿~本文我将整理网上搜到的资料，谈谈我的理解，我自认为是循序渐进的方式谈协方差矩阵，希望一篇文章，讲清它的来龙去脉。有不明白的地方，欢迎指出，我们一起探究。

### 统计学的基本概念
学过概率统计的孩子都知道，统计里最基本的概念就是样本的均值，方差，或者再加个标准差。首先我们给你一个含有n个样本的集合，依次给出这些概念的公式描述，这些高中学过数学的孩子都应该知道吧，一带而过。

均值：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/185435_4fcc80d1_5631341.png "屏幕截图.png")

标准差：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/185443_f80d35fa_5631341.png "屏幕截图.png")

方差：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/185449_2ea77ac5_5631341.png "屏幕截图.png")

很显然，均值描述的是样本集合的中间点，它告诉我们的信息是很有限的，而标准差给我们描述的则是样本集合的各个样本点到均值的距离之平均。以这两个集合为例，[0，8，12，20]和[8，9，11，12]，两个集合的均值都是10，但显然两个集合差别是很大的，计算两者的标准差，前者是8.3，后者是1.8，显然后者较为集中，故其标准差小一些，标准差描述的就是这种“散布度”。之所以除以n-1而不是除以n，是因为这样能使我们以较小的样本集更好的逼近总体的标准差，即统计上所谓的“无偏估计”。而方差则仅仅是标准差的平方。

这就好比，每年网上都会报XXX年人均收入，这个反映的是均值，我们每个人的收入就是一个样本，一看收入的第一反应，就是作差，又又拖后腿了，差值有正有负，平方求和/(n-1)），这反应的就是贫富差距，方差越大，社会财富分散度越集中，贫富差距越大。

 **为什么需要协方差？** 

上面几个统计量看似已经描述的差不多了，但我们应该注意到，标准差和方差一般是用来描述一维数据的，
最简单的大家上学时免不了要统计多个学科的考试成绩。面对这样的数据集，我们当然可以按照每一维独立的计算其方差，但是通常我们还想了解更多，但现实生活我们常常遇到 含有 DUO 维 数据的 数据集，
比如，一个男孩子的猥琐程度跟他受女孩子欢迎程度是否存在一些联系啊，嘿嘿~协方差就是这样一种用来度量两个随机变量关系的统计量，我们可以仿照方差的定义：

方差的定义

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/185500_a0501c2c_5631341.png "屏幕截图.png")

来度量各个维度偏离其均值的程度，标准差可以这么来定义：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/185509_a308fc60_5631341.png "屏幕截图.png")

协方差的结果有什么意义呢？如果结果为正值，则说明两者是正相关的(从协方差可以引出“相关系数”的定义)，也就是说一个人越猥琐就越受女孩子欢迎，嘿嘿，那必须的~结果为负值就说明负相关的，越猥琐女孩子越讨厌，可能吗？如果为0，也是就是统计上说的“相互独立”。

其实从协方差的定义上我们也可以看出一些显而易见的性质，如：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/185525_cbcd75ec_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/185532_17a93455_5631341.png "屏幕截图.png")

### 协方差矩阵
协方差多了就是协方差矩阵，上一节提到的猥琐和受欢迎的问题是典型二维问题，而协方差也只能处理二维问题，那维数多了自然就需要计算多个协方差，比如n维的数据集就需要计算

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/185554_e6cd0a69_5631341.png "屏幕截图.png") 个协方差，那自然而然的我们会想到使用矩阵来组织这些数据。

于是就有了协方差矩阵： ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/185603_b0ec5b79_5631341.png "屏幕截图.png") 其中 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/185610_5702dbe1_5631341.png "屏幕截图.png")

这个定义还是很容易理解的，我们可以举一个简单的三维的例子，假设数据集有三个维度，则协方差矩阵为

![输入图片说明](https://images.gitee.com/uploads/images/2022/0116/185618_2ad12961_5631341.png "屏幕截图.png")



可见，协方差矩阵是一个对称的矩阵，而且对角线是各个维度上的方差。

协方差矩阵通常这样计算，先让样本矩阵中心化，即每一维度减去该维度的均值，使每一维度上的均值为0，然后直接用新的到的样本矩阵乘上它的转置，然后除以(N-1)即可。其实这种方法也是由前面的公式通道而来，只不过理解起来不是很直观，但在抽象的公式推导时还是很常用的！

 **参考文档** 

http://pinkyjie.com/2010/08/31/covariance/

https://janakiev.com/blog/covariance-matrix/

发布于 2019-10-15 18:01

# 【笔记】#[21 小觅智能庞博士 VSLAM](https://gitee.com/yuandj/siger/issues/I3Y7Y0)

- 《小觅新视界》是给庞博士的作业命题

  - 《 **你想进入AI行业中最热门的专业拿到百万年薪吗？** 》[关键帧](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8315741_link) [字幕全文](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8319206_link)

    - face
    - State of art
    - 飞控PILOT
    - [minteye 小觅双目](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8319285_link) [秦通](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8319946_link) 惯导
    - XILINX
    - darpa机器人2015
    - [AStar.ai](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8324611_link) 鱼眼视觉,专利
    - VSLAM 高阶课程，多传感器融合，[VIO](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8324629_link)，用上小觅的VIO，VSLAM，
    - 3DMAPPING&OVERVIEWING，商业精度，启动定位。
    - [王齐助教](https://gitee.com/yuandj/siger/issues/I3Y7Y0)

  - [该问题是怎么引起的？](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8324326_link)  
    bing `What is Visual SLAM? What does Visual SLAM Mean ...`

    - 什么是视觉Visual SLAM - 知乎

- [bing `StanfordRobotics.AI`](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8319271_link)  
  [商业计划 - 【机器视觉】史丹福-天使轮商业计划书BP V4.1.pdf](https://max.book118.com/html/2021/0721/7130164124003146.shtm)
  
- [bing `小觅智能，MYNT`](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8319274_link)

  - [36氪首发 | 「MYNT AI小觅智能」获蜂网投资千万级pre-B轮追加融资，加速3D视觉感知技术落地](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8319275_link) [[笔记](https://36kr.com/p/1724767846401)]

  - [MYNT EYE小觅双目摄像机开箱试用全记录](https://zhuanlan.zhihu.com/p/141850110) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8319280_link)]

    <p><img src="https://images.gitee.com/uploads/images/2022/0116/031844_5ab405d0_5631341.png" height="69px"></p>

  - [小觅双目摄像头深度版(D版)常见问题](http://support.myntai.com/hc/kb/article/1226610) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8319285_link)]

    - SDK安装问题 （[SDK文档](https://mynt-eye-d-sdk.readthedocs.io/zh_CN/latest/index.html)）
    - SDK及适配的slam开源项目地址： [GitHub](https://github.com/slightech) [码云](https://gitee.com/mynt/MYNT-EYE-D-SDK)
      > [如果无法从github下载代码或者下载网速过慢，可以使用码云](https://gitee.com/mynt/MYNT-EYE-D-SDK)。

  - [小觅深度相机标准版 ROS使用](https://blog.csdn.net/qq_23076153/article/details/122457299) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8319287_link)]

    <p><img src="https://images.gitee.com/uploads/images/2022/0116/035559_2d58511b_5631341.png" height="69px"></p>

  - [SLAM学习 | 使用小觅相机MYNTEYE-S1030收集数据集](https://blog.csdn.net/ctyqy2015301200079/article/details/107821371) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8319288_link)]
    
    <p><img src="https://images.gitee.com/uploads/images/2022/0116/040715_77d6465b_5631341.png" height="69px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3 总结与评价</p>

    总地来说这款MYNTEYE-S1030-IR相机还是不错的，同时搭载了双目视觉、双通道深度相机和IMU传感器，并实现了相机的时间配准，同时SDK也比较完备，确实是一款新手友好型SLAM学习工具。

    但是问题也是有不少的。

    - 第一，光学成像质量较差，例如从图2就能看出成像有明显的白色斑点（虽然并非每次都会出现，但是一旦出现，当次数据就算全都报废了）；
    - 第二，成像较小，还是752*480的灰度图；
    - 第三，公司疑似倒闭，几乎没有技术支持（好惨）。

    最后可能就是一个玄学问题：遇到问题记得拔插USB试试，万一它就好了呢 :dog: 

  - [MYNTAI全新产品系AI超算平台正式上线](https://www.sohu.com/a/256765286_320333) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8319290_link)] <img src="https://images.gitee.com/uploads/images/2022/0116/041417_e81a0379_5631341.png" height="19px">

- [华为天才计划 200万年薪 香港科技大机器人专业的秦通，MYNTeye 小觅智能用户](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8319946_link)

  > [引自：天才“吸铁石”华为新添俄罗斯00后编程冠军](https://gitee.com/yuandj/siger/issues/I4QR64#note_8319848_link)

  <p><img src="https://images.gitee.com/uploads/images/2022/0116/105845_278056d9_5631341.png" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/111449_c751488c_5631341.png" height="69px"></p>

  - [“天才少年”秦通 港科大硕博连读](http://www.takungpao.com/news/232108/2020/0805/483292.html)
  - [华为年薪百万的应届博士到底有多厉害？](https://www.zhihu.com/question/336447092) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8320044_link)]

  [任正非曾在华为EMT（经营管理团队）内部讲话中提及，将从全世界招进20-30名“天才少年”，“这些‘天才少年’就像‘泥鳅’一样，钻活我们的组织，激活我们的队伍”。](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8320078_link) —— [刚毕业年薪201万！华为最高档“天才少年”火了，全球仅4人](https://zhuanlan.zhihu.com/p/167117611)  **旧闻略** 

  - 【泡泡机器人公开课预告】[秦通-视觉惯导里程计时差的在线标定](https://www.sohu.com/a/260362311_715754) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8320104_link)]
    - [泡泡机器人SLAM公开课链接（实时更新中）](https://blog.csdn.net/qq_34622997/article/details/88085884) [[转载](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8320130_link)] 共116期
      - [100.视觉惯导里程计时差的在线标定-秦通](https://pan.baidu.com/s/1BQW7UNJimpo50fDVZHZJhg) 密码:k4z8 [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8320652_link)]

- [bing `三维视觉的历史起源和演变`](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8320710_link)

  - [机器视觉发展史](https://www.cnblogs.com/jiqishijue/archive/2011/12/10/2283503.html)

  - [三维视觉 - 知乎](https://www.zhihu.com/topic/20232180/hot) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8320768_link)]

    - 如何看待计算机视觉未来的走向? <img src="https://images.gitee.com/uploads/images/2022/0116/134626_416d82d7_5631341.png" height="19px">

      > 在搜索引擎，智能对话领域，CV越来越多地和NLP相结合，去做多模态的语义理解及信息检索。  
      > 在语音领域，CV也正在和语音融合，进行多模态地识别和降噪及生成。  
      > 在自动驾驶领域，视觉也在和激光雷达，毫米波雷达等传感器实现传感器融合获得更精准的算法结果。  
      > 计算机视觉将会和各个领域融合不断创造出新的子领域，而各个子领域又将不断深入挖出新的场景和应用，产出新的价值。深度学习时代，所有的数据都能用tensor表示，那既然如此，何不组合创新出新的玩法呢？

- [bing `三维视觉传感器的三大技术路线和应用场景`](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8320845_link) 

  [超详细的3D视觉技术学习路线](https://zhuanlan.zhihu.com/p/97299116)

  162 人赞同了该文章
  > 本文首发于公众号【3D视觉工坊】，原文请见3D视觉系统化学习路线
  > 欢迎加入国内最大的3D视觉交流社区，1700+的领域从业者正在共同进步~
更多干货资源：

  [汇总 | 国内最全的3D视觉学习资源，涉及计算机视觉、SLAM、三维重建、点云处理、姿态估计、深度估计、3D检测、自动驾驶、深度学习（3D+2D）、图像处理、立体视觉、结构光等方向！](https://mp.weixin.qq.com/s/xyGndcupuK1Zzmv1AJA5CQ)

  - [3D视觉从入门到精通系统学习教程](https://mp.weixin.qq.com/s/xyGndcupuK1Zzmv1AJA5CQ) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8321323_link)]

    <p><img src="https://images.gitee.com/uploads/images/2022/0116/150720_b2f87194_5631341.png" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/153509_31a5ee54_5631341.png" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/153539_47213437_5631341.png" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/153549_cf473b72_5631341.png" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/153605_d7c1878c_5631341.png" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/154349_b2f2ca6c_5631341.gif" height="69px"></p>

    - [「3D视觉从入门到精通」知识星球](https://images.gitee.com/uploads/images/2022/0116/153712_2631b551_5631341.png) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8321494_link)]

  - [视觉传感器的工作原理及应用场景分析](https://www.eefocus.com/sensor/428674) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8321595_link)]

- [机器之眼](https://www.zhihu.com/column/c_1160922297967312896) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8322947_link)]

  > 给工业机器人一双眼睛，它能实现智造  

    - [3D相机---激光三角测量方案](https://zhuanlan.zhihu.com/p/88301570) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8322995_link)]
    - [机器视觉绕不过的数学问题(一)--协方差矩阵](https://zhuanlan.zhihu.com/p/86624275) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8323019_link)]
    - [3D相机（二）--TOF相机](https://zhuanlan.zhihu.com/p/85519428) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8323376_link)]
    - [3D相机（一）---立体视觉传感器](https://zhuanlan.zhihu.com/p/85517554) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8323378_link)]

  [敏感词再次回报了我，看下面的这些图就可以了解了。 BING `BAOWEIER TOUJING`](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8323510_link)
    
    <p><img src="https://images.gitee.com/uploads/images/2022/0116/202139_f3b73901_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202146_6808a2d9_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202150_b31cdb32_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202156_b4bd04d5_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202202_1e9ce586_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202207_b48462db_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202215_d2860229_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202221_31588502_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202228_578ed33a_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202235_d11fa1f4_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202245_c075982e_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202251_a4f7987e_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202257_ba3735a0_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202304_4d8332e6_5631341.png"  height="29px"> <img src="https://images.gitee.com/uploads/images/2022/0116/202313_440cb840_5631341.png"  height="29px"></p>

- [素材](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8324128_link) - [How to render depth of field faster](https://www.creativebloq.com/3d/how-render-depth-field-faster-41411254) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8323994_link)]
  
  <p><img src="https://images.gitee.com/uploads/images/2022/0116/221829_fdced8df_5631341.png"  height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/223913_e5207d0c_5631341.jpeg" title="R-C.jpg" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/224005_31627869_5631341.png" title="photoscan_outdoor2_4.png" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/224027_f0880e05_5631341.jpeg" title="What-are-the-application-scenarios-of-ToF-sensor.jpg" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/224047_eff05b90_5631341.png" title="R-C.png" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/224120_6567f956_5631341.jpeg" title="R-C (1).jpg" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/224133_85c0065a_5631341.jpeg" title="depthsensing-1024x811.jpg" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/224436_62b0725e_5631341.png" title="week2-2-depthmap-1.png" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0116/224502_a737292a_5631341.jpeg" title="usbsmsfx5dsmw8wf4m5t.jpg" height="69px"></p>

- [31 事实求视](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8324427_link)

- [Astar.ai](https://astar.ai/) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8324611_link)] | [CaliCam®](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8324622_link) | [SLAM Technology 技术](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8324627_link)

  <p><img src="https://images.gitee.com/uploads/images/2022/0117/025317_5b9102ac_5631341.png"  height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0117/030141_06b03e98_5631341.png"  height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0117/030344_45254204_5631341.png"  height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0117/030412_7caac1c7_5631341.png"  height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0117/030925_977002f6_5631341.png"  height="69px"></p>

- [SLAM/VIO学习总结](https://zhuanlan.zhihu.com/p/34995102) [[笔记](https://gitee.com/yuandj/siger/issues/I3Y7Y0#note_8324629_link)]
  
  <p><img src="https://images.gitee.com/uploads/images/2022/0117/032547_02dc998d_5631341.png" title="单目稠密重建地图进行无人机避障和路径规划" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0117/032632_c9087258_5631341.png" title="自动驾驶高精地图示例" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0117/032640_6b7032cd_5631341.png" title="高精地图地图采集车" height="69px"></p>

# 心手合一一起建造新视界

@[袁德俊](https://gitee.com/yuandj) :   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://images.gitee.com/uploads/images/2022/0116/234300_0e861965_5631341.png" title="2022-1-16 23:39 吉祥数独31.png" height="169px">

> VSLAM专题已经备好稿件，只待编纂啦，不像错过这份喜悦要与您分享，鸽了半年的作业就要完成了。[合十]，希望得到老师的鼓励，再能加把油。

> 这是用我们的FPGA开发板算出来的一组六宫数独，题面有特殊意义，曰吉祥数独，用来新年送祝福的。完成此套题一气呵成，即可来年平顺安康[合十] 送给庞博士，有缘能相识，也祝愿您阖家幸福，2022更上一层楼。

> SIGER 期刊2021年度的选题 VSLAM是最后一个，也作为2022最IN的主题推荐给更多同学，今天特地向您要个祝福语2-4个字即可，作为鼓励。并把这个祝福和第31组题结合，以示纪念。 https://gitee.com/flame-ai/siger 

> 并邀请您做为SIGER的志愿者导师[合十] 有同学提问，转达给您。这是VSLAM的学习笔记
https://gitee.com/yuandj/siger/issues/I3Y7Y0

> <p><img src="https://images.gitee.com/uploads/images/2022/0117/021247_ce8f2936_5631341.png" height="169px"> <img src="https://images.gitee.com/uploads/images/2022/0117/021329_27f06269_5631341.png" height="169px"> <img src="https://images.gitee.com/uploads/images/2022/0117/022148_04ca66e5_5631341.png" height="169px"></p>
> 2022新选题中，垂直起降飞机 eVTOL 可以是一个，您介绍的一位有着飞行员爸爸的9岁男孩的故事，也非常适合分享。这就是SIGER导师的引领作用 :pray: 

@[庞博士](https://images.gitee.com/uploads/images/2022/0117/015719_b080973f_5631341.png)  **实事求视** 

> 这份鼓励我们收到了，是希望同学们用实践检验所学的意思。前两天征集的一个选题 “[耳听佳音](https://gitee.com/yuandj/siger/issues/I4QWTB)” 加上我们的新视界，这是耳聪目明的意思啊。而我是想让孩子们用心去感知这个世界，因为用心了，耳目自然就聪明了，自然就能看到想看的和想听的啦，而世界也会因他们而变的更好 :pray: 这就是我开始今天的述说，[火焰棋 <img src="https://images.gitee.com/uploads/images/2022/0117/215843_1b8d437c_5631341.png" height="19px" title="flamechess.cn"> 的故事](http://flamechess.cn/?p=75) 啦... :pray:

### 大作业： Building a flamechess tower by heart.

> 火焰棋实验室的根是社区，火焰棋则是来自社区的灵感，它从一个陪娃游戏演化成一个分享爱的社区公益活动，再到如今肩负科普使命的公益项目，都源于一个承诺 “让火种传递的远些，更远些！”，这是我带着[火焰棋从丹麦回来后](http://flamechess.cn/?p=20)给社区的家长们的承诺。而就是这句承诺，激发出的灵感，我们（和家长们）一起造了这个庞然大物，5米高的史上最高的国际象棋棋子。我们叫它 “[火焰棋塔](http://flamechess.cn/?p=75)”，它是一个丰碑，是一同分享的明证。它更像是有一种力量，吸引着人们有了更大地 “期待”，能造的更多，更大，于是有了 “火焰棋塔阵”，一个40米见方，34座高塔的巨型棋盘，而且它就差一步就要完成啦...

<p><img src="https://images.gitee.com/uploads/images/2022/0117/233229_1e0c40be_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0117/233243_9f237392_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0117/233254_0e343085_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0117/233317_619bbfcd_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0117/233306_8747cd20_5631341.jpeg" height="99px"><br><img src="https://images.gitee.com/uploads/images/2022/0117/233412_6b9bd516_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0117/233421_a5cb48ca_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0117/233401_f207fc7c_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0117/233351_8a7fb7e0_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0117/233430_f760b918_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0117/233328_48504af8_5631341.jpeg" height="99px"></p>
 
时光机快进，这个念想多年萦绕在心的我，出现在了庞博面前，希望他能助我完成心中的 “火焰棋塔阵”，它是：

- 一个存在于火种志愿者心中的丰碑，我们都希望它能建成
- 我们有了已经建造了6座实体火焰棋塔，等比例高差，均对标第一座5米的 QUEEN
- 新塔有一个巧妙的设计（三十厘米边长的三愣柱，由5层的瓦楞纸板弯折而成）可以自由地拼插成型
- 现在，我们希望用 AR 技术实现他，从建造第一块三愣柱开始，直到我们汇聚在现场或者连线介入视界：
  - 我们会邀请棋王和棋后，同时现身，执掌手中的棋子去控制这盘世界最大棋盘
  - 他们的每个落子，都是身后万千小火种的选择，他们玩法是火焰棋，[每吃掉一个子会有一块蛋糕入孩子们的口](http://flamechess.cn/?p=426)
  - 我们的第一现场选择，就是鸟巢，因为第一座火焰棋塔，是在鸟巢旁边树立的。:pray:

上作业：

- 所有参与其中的人，都会有一套AR系统，他们眼中看到的就是我们心中的火焰棋塔巨阵
  - 我们需要集合所有人力，能够对我们选择的场地进行高清建模，能保证每个参与者的视角内可以逼真地还原场景，就像他们真地在一块一块地搭建着，到组装，测试，彩排，实战表演
- 我们的志愿者TEAM分为建造组和测试组
  - 建造组主要是前期介入的同学，使用任何可以使用的设备对现场扫描，扮演各个角色，从不同视角进行
  - 建造组较测试组的计算设备要先进一些
  - 扫描建模工作一段时间后，就可以开始实验性建造，尽管环境还不是很完美。
  - 我们需要一个集中数据的服务器，并能有循环的进程对数据不断修复，逐步逼近可以表演的要求
- 测试组AR设备只需要两点能力：
  - 能够同步精确位置信息
  - 完美渲染
- 建造组AR设备还需要有扫描计算能力：
  - 3D 立体相机一定是需要的前端
  - 如何使用 SLAM 技术完成理想的 3DMAPPING ？ 有待学习研究
- 现场渲染有两个方案
  - VR方案，能够通过实现建造的场景，重新渲染，适合远程连线的观众
  - AR方案，现场通过看台将虚拟棋盘和实景叠加，渲染引擎？有待学习和兼有
- 个人版巨型AR棋盘方案（研学方案，为正是方案储备骨干志愿者。）
  - 基于现有 石榴派 的AR 三维视觉解决方案
  - 增加 双通道摄像头，实现立体视觉算法，达到三维立体相机的效果
  - 使用开源的AR渲染方案，能够在精度要求不高的场景，浮现巨型AR棋盘
  - 两个级别:
    1. 只用于视频录制，
    2. 能有 头戴AR设备，操作场景中的棋子，精度要求相对降低
