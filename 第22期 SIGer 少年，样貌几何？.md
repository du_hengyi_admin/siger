[![输入图片说明](https://images.gitee.com/uploads/images/2021/1223/153120_7e7e7b29_5631341.jpeg "后翼弃兵2副本999.jpg")](https://images.gitee.com/uploads/images/2021/1223/153100_00fe8510_5631341.jpeg)

# [SIGer少年，样貌几何？](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM)

—— 基于AI的智能头像与照片墙自动生成技术背景科普 @[李浩泽](https://gitee.com/LHZ_CUC)

每一位接触SIGer期刊的读者，或SIGer期刊的编辑，心中或许都会有一个这样的疑问：什么是SIGer少年？我们心中的SIGer少年，如果他是一个具象化的人，应该是什么样的？一千个观众眼中有一千个哈姆雷特，我们无法真正做出一个完备的定义，但基于我们SIGer期刊一直以来的AI理念，这个疑问的答案或许能够真正地得到一个具象化表达。在本文中，笔者将带着这个疑惑与期待，与各位读者一同探索SIGer少年画像背后的技术——基于AI的图像识别、智能图像调整、智能拼接、词云图技术等，希望与读者朋友们一同在心中构建起属于SIGer少年的画像！

- 图像识别/拼接/修复/着色
- 词云图技术

> 在阅读了SIGer少年画像背后的技术之后，各位读者是否对这张画像有了自己的见解呢？相信各位读者在心中已经可以绘画出属于自己的SIGer少年画像了。袁老师对我说：我希望 SIGer 能成为一个少年的样子，不需要多么清晰的五官，一眼看上去即是少年，符合大家心中少年的模样。我们心中也是如此想法。我希望我们可以用属于SIGer的AI血液，真正制作出我们心中的那一幅SIGer少年画像。感谢各位读者的阅读！

- 请阅全文：[STEM](STEM/)/[SIGer少年，样貌几何？.md](STEM/SIGer少年，样貌几何？.md)

---

SIGer少年，样貌几何？【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM)】

- [斧正邀请](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7837905_link) @yuandj [点评](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870453_link)

  > 这篇原创文章恰到好处地将AI画像的各个技术点做了串接，形成了完美的学习曲线，如上列出的关键字，每一个都足够同学们消化理解和学习的。先记录下来，伴随 SIGer AI 画像的完成，这次学习之旅才能告一段落，期待同学们踊跃加入，共同学习吧。我仿佛已经看到了未来 AI 生成的 SIGer 画像是什么样貌啦。感谢 @LHZ_CUC 同学的分享。

    - 遗传算法与BP网络相融合，
    - 角点的邻域
      - Harris角点检测算法、
      - SIFT (Scale Invariant Feature Transform),
      - FAST (????)
      - SURF (Speeded-up robust feature)
    - 整体变分方法 (TV，TotalVariational) 
      - 欧拉-拉格朗日方程 
      - 基于曲率的扩散模型 (CDD，Curvature-DrivenDiffusion)

- [全部参考文献](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870495_link)：

  > 以上文献的翻阅，也花费了 @LHZ_CUC 同学大量的时间，这份分享是沉甸甸的。这也是 SIGer 编委的一个福利，试想如果是同学你主笔调研，那这份学习收获就属于你啦。:pray:

    1. 《[智能图像识别初探系列（一）](https://blog.csdn.net/yoggieCDA/article/details/105100011?ops_request_misc=&request_id=&biz_id=102&utm_term=%E6%99%BA%E8%83%BD%E5%9B%BE%E5%83%8F%E8%AF%86%E5%88%AB&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-0-105100011.first_rank_v2_pc_rank_v29&spm=1018.2226.3001.4187)》（[全文](https://blog.csdn.net/yoggieCDA/article/details/105100011)）【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870715_link)】
    2. 《[人工智能-图像识别](https://blog.csdn.net/iFlyAI/article/details/89379357?ops_request_misc=&request_id=&biz_id=102&utm_term=%E6%99%BA%E8%83%BD%E5%9B%BE%E5%83%8F%E8%AF%86%E5%88%AB&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-3-89379357.first_rank_v2_pc_rank_v29&spm=1018.2226.3001.4187)》（[全文](https://blog.csdn.net/iFlyAI/article/details/89379357)）【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870748_link)】
    3. 《[计算机视觉方向简介|图像拼接](https://blog.csdn.net/electech6/article/details/95996610?ops_request_misc=&request_id=&biz_id=102&utm_term=%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E5%9B%BE%E7%89%87%E6%8B%BC%E6%8E%A5&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-1-95996610.first_rank_v2_pc_rank_v29&spm=1018.2226.3001.4187)》，（[全文](https://blog.csdn.net/electech6/article/details/95996610)）【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870768_link)】
    4. 《[随机抽样一致算法（Random sample consensus，RANSAC）](https://www.cnblogs.com/xingshansi/p/6763668.html)》（[全文](https://www.cnblogs.com/xingshansi/p/6763668.html)）【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870778_link)】
    5. 《[SLAM入门之视觉里程计(5)：单应矩阵](https://www.cnblogs.com/wangguchangqing/p/8287585.html)》（[全文](https://www.cnblogs.com/wangguchangqing/p/8287585.html)）【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870781_link)】
    6. 《[AI 图像智能修复老照片，效果惊艳到我了！| 附代码](https://blog.csdn.net/csdnnews/article/details/105828632?ops_request_misc=&request_id=&biz_id=102&utm_term=%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E5%9B%BE%E5%83%8F%E4%BF%AE%E5%A4%8D&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-1-105828632.first_rank_v2_pc_rank_v29&spm=1018.2226.3001.4187)》（[全文](https://blog.csdn.net/csdnnews/article/details/105828632)）【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870786_link)】
    7. 《[使用OpenCV和深度学习对黑白图像进行着色](https://blog.csdn.net/learning_tortosie/article/details/89076984)》，W_Tortoise（[全文](https://blog.csdn.net/learning_tortosie/article/details/89076984)）【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870801_link)】
    8. 《[[机器学习实战] 深度学习为黑白图像着彩色](https://blog.csdn.net/soulmeetliang/article/details/78002962)》，holeung（[全文](https://blog.csdn.net/soulmeetliang/article/details/78002962)）【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870808_link)】
    9. 《[参考近百篇文献，“图像着色”最全综述](https://blog.csdn.net/bevison/article/details/108271806?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163888610416780265450772%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=163888610416780265450772&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v31_ecpm-2-108271806.first_rank_v2_pc_rank_v29&utm_term=%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E5%9B%BE%E5%83%8F%E7%9D%80%E8%89%B2&spm=1018.2226.3001.4187)》，OpenCV中文网公众号（[全文](https://blog.csdn.net/bevison/article/details/108271806)）【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870824_link)】
    10. 《[一篇关于如何用深度学习完成自动上色（Automatic Image Colorization）的论文浅析](https://blog.csdn.net/u010030977/article/details/78846198?utm_term=%E6%B7%B1%E5%BA%A6%E5%AD%A6%E4%B9%A0%E5%9B%BE%E5%83%8F%E7%9D%80%E8%89%B2&utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~sobaiduweb~default-0-78846198&spm=3001.4430)》，Stark_xhz（[全文](https://blog.csdn.net/u010030977/article/details/78846198)）【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870841_link)】

- [SIGer AI 画像项目缘起](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7870610_link)：

  > 我希望 SIGer 能成为一个 少年 的样子，不需要这么清晰的五官，一眼看上去就是少年，符合大家心中 少年的样子，合成的元素，可以是 SIGer 期刊封面，也可以是词云，也可以是任何其他素材。这样的AI独有的景象，将可以非常强地区分出，我们 SIGer 的AI风貌。与传统媒体的区别。甚至可以为这个主题，制作一期 B 站的视频。详细介绍其中的技术。只有图和论文，不过瘾，不如视频有冲击力。

  - [定稿 The Queen's GamBit](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7871084_link) 《女王的开局》【[原图](https://images.gitee.com/uploads/images/2021/1215/234247_9bc419f2_5631341.jpeg)】

  - [第一稿本期封面](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7871101_link)【[图](https://images.gitee.com/uploads/images/2021/1215/234502_a729a507_5631341.jpeg)】

  - [bing 后翼弃兵翻译](https://zhuanlan.zhihu.com/p/352697969)【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7871111_link)】[中文海报](https://images.gitee.com/uploads/images/2021/1215/235521_58808831_5631341.jpeg)

    > 不过“王翼弃兵”容易造成王翼防守能力下降，人们往往较多采用“后翼弃兵”。 这也就是“The Queen's Gambit”的意思。 剧名《The Queen's Gambit》翻译成《后翼弃兵》比较符合国际象棋术语，但不如翻译成《女王的棋局》切题。做我所爱成为冠军——《后翼弃兵》 - 知乎  

  - [一幅特殊的肖像画，献给世界冠军赛！](https://mp.weixin.qq.com/s?__biz=MzkxMTIyMTkwMw==&mid=2247491329&idx=1&sn=a3e784d7bb9927957755381b0288665d)【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7871129_link)】

  - [包云岗：总要有人去做一些不同的事](https://gitee.com/flame-ai/siger/blob/master/RISC-V/baoyungang%2010%20years%20diffrent.md)【[笔记](https://gitee.com/LHZ_CUC/siger/issues/I4MEKM#note_7965311_link)】[国重十周年海报](https://images.gitee.com/uploads/images/2021/1221/214358_90cd6afb_5631341.png)

    > [经典的照片墙艺术加工](https://images.gitee.com/uploads/images/2021/1221/214358_90cd6afb_5631341.png)，像 是艺术的直接表达。期待 SIGer 画像早日诞生！

---

### 广而告之：[“吉牛象棋火种队” 招新啦](https://gitee.com/blesschess/luckystar/issues/I4KIX5) :pray:

本期刊发时逢国际象棋世界冠军赛，全球瞩目，期间的特殊肖像画也和本期的技术主题相关，但它的文化意义更重要，选择封面时为具象地说明 SIGer 海报 AI 自动生成系统的技术特点，也选择了《女王的开局——后翼弃兵》为封面，暗合火焰棋AI实验室的缘起 国际象棋 社区活动。以 SIGer 的开源成果 —— [石榴派](https://gitee.com/shiliupi/shiliupi99) 武装的社区志愿者，将成为 社区象棋火种队的中坚。借用 [ShiliuPi](http://shiliupi.cn) 的第一个应用 [GNUchess](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2011%20-%20GNUgames%2099.md) [吉牛99](https://images.gitee.com/uploads/images/2021/0730/124009_f42a3b5f_5631341.jpeg) 为名，讨个好口彩，特命名为 吉牛象棋火种队，预祝小朋友们取得好成绩，都能成就自己的冠军梦！——  _[我们都是宇宙的中心 :D](http://shiliupi.cn/2021/12/14/womenshiyuzhoudezhongxind/)_ 

- 吉牛象棋火种队的开源阵地：[blesschess/luckystar](https://gitee.com/blesschess/luckystar/)

  - [BEST WORD 99 was borned.](https://gitee.com/blesschess/luckystar/issues/I4D7DW) 石榴派的传统，征集99寄语成功后，BestWords 是当前活动
  - [“吉牛象棋火种队” 招新啦](https://gitee.com/blesschess/luckystar/issues/I4KIX5) :pray: 本广而告之主贴，《[棋盘上的智慧](https://images.gitee.com/uploads/images/2021/1201/073912_9c67787b_5631341.jpeg)》是主题
  - [LUCKY-LUCKY 喜上加囍](https://gitee.com/blesschess/luckystar/issues/I4MXKG)，打通全栈的 AI 应用 吉祥数独 即将上线，吉牛火种队的王牌应用 :pray: