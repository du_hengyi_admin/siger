[![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/164500_a9b9a759_5631341.jpeg "9781554812721副本999.jpg")](https://images.gitee.com/uploads/images/2022/0104/164430_aea8c050_5631341.jpeg)

## [数学之美《丘成桐传》](https://gitee.com/yuandj/siger/issues/I3HTQ7)

### 缘起

一篇《[全国第一！7名巴蜀学子获 2021 年清华大学 “丘成桐” 数学科学领军人才 培养计划、数学英才班 入围认定](https://mp.weixin.qq.com/s?__biz=MjM5ODMwNDI0MQ==&mid=2650772882&idx=1&sn=97a4023ddc638b46f605536c318e0a2a)》的文章进入眼帘，是我们人工智能学会机器博弈专委会老师的转发，注解 “别人家的孩子！” 。什么时候，别人家的孩子成为了品学兼优的代名词，多少有些让人 “焦虑” ，这是背离学习的目的的，不符合 SIGer 的价值观。就数学学习，我有一个自己的体会，2012 年雅安地震的时候，我带着火焰棋的全部研究成果，募集了 20 盒 “器材” 去灾区教小朋友玩游戏，DIY 棋具分享给更多帐篷中玩耍的小伙伴。当时，我驻扎在一个救援队的帐篷中，深夜闷热，也或许心中不能平复的心情，和值更的救援队长分享起白天的活动场景。我至今记得队长告诉我的故事：

> 我家闺女小学三年级的时候，数学成绩也很 “要命”，能到不及格的地步，我就花了 1 个月时间每天吃完晚饭，玩一小时的 “算24” （后来听数独协会的老师分享，这是咱们中国发明的游戏，随数学奥赛队员远征国际） _虽然括号里的话有些说故事的嫌疑，但不可否认，游戏在数学教学中的作用。_ 结果不用说，四年级之后，数学就再也没操过心。

当然，立这个专题，不是算24，也不是火焰棋，而是另一篇 [丘成桐自传《我的几何人生》读后感](https://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652726452&idx=2&sn=1f2731027638ee2db293e4898b0103da) ：

> 他深知，第一流的学问，第一流的事业，往往需要贯通原本相距遥远的事物。
> 所有的珠子，最后都串成了线。

就是这句话打动了我，这就是 SIGer 现在正在做的事情，我们不是要禁锢孩子们的求知欲，而是要给予他们更多的鼓励和营养，去激发他们去探索星辰大海，求知无穷的宇宙奥秘，我分享给 和乐数学 的编辑老师时，就是这样讲的。写下算24的故事，也是有感而发。我希望在接下来的日子里，以集中转载，成数学专题的方式，推荐 和乐数学 前期的耕耘，为更多同学们打开数学世界尽一份力。也期望 SIGer 学习社群，能够起到桥梁作用，助更多少年通往成功之路，共建美好未来。

### 相关转载

此前和乐转发的文章
- [清华将成立求真书院，丘成桐要培养基础学科“八百铁骑”](http://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652725568&idx=2&sn=ff5e3a167eb7942564abc2bca7abb3be)
- [不拘一格！清华将致力于培养顶尖数学家](http://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652724217&idx=2&sn=92625a41968be036ed21d1e8df6a2851)

《[清华丘成桐数学英才及领军人才名单](https://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652726489&idx=1&sn=4950d6dd8eb937eb301e011aa6ca9270)》[和乐](https://images.gitee.com/uploads/images/2021/0407/235727_1614151a_5631341.png)

- [丘成桐专访谈新书：我不愿意造假，每天游泳2小时](http://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652725651&idx=1&sn=dd6b1cd02f66980d83466d78a866ca75)
- [丘成桐自传开售 | 我的几何人生](http://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652725592&idx=1&sn=af1d03d6a19c9a22c9f6db05f27361ed)
- [丘成桐自传《我的几何人生》读后感](http://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652726452&idx=2&sn=1f2731027638ee2db293e4898b0103da)

![输入图片说明](https://images.gitee.com/uploads/images/2021/0407/235339_510e29cc_5631341.png "屏幕截图.png")

我在1月8日转载过一篇，也是我带同学们开启学开源假期社会实践活动的时的分享。
- [无须高考 清华“丘成桐数学科学领军人才培养计划”来了](http://m.news.cctv.com/2021/01/08/ARTIVfYtPhEjqXR6sEgmxg60210108.shtml)

> 非高中，也就是少年班啦，初三小同学直接上清华。第一次听到丘成桐，还是小榕树基金，清华附中学生节上的一次公益跑，我好奇，这是怎样的活动，跑五圈，就能带为捐赠五元。后来一问，是清华附中的一位同学，参加丘成桐竞赛，获奖16万元，悉数捐赠母校设立助学基金。当时我深为感动，和兮兮利用接下来的六一节组织同学游戏募捐了一些零钱捐给了小榕树，也因此和大姐姐成为笔友。今天看到丘成桐大师的招募信息，来自某“科学教师”，看到标题“无需高考”，觉得被误读啦，教育本无错，只是离初心越来越远啦。这样的特殊管道，引出的孩子是幸运的。本就不凡，能有这样一个通道被呵护，成长更茁壮。

### 【[笔记](https://gitee.com/yuandj/siger/issues/I3HTQ7)】

[【专题】数学之美 #17 “和乐数学” 《丘成桐传》](https://gitee.com/yuandj/siger/issues/I3HTQ7)

> [美国报刊将其称为“数学王国的凯撒大帝”，其中暗含对他个性的概括：不屈不挠，勇往直前。这种个性显然是他成功的重要基石。](https://gitee.com/yuandj/siger/issues/I3HTQ7#note_4890901_link)

> [就差《和乐之美》的推介了。公号MAP + 文章列表 :+1: 即可。](https://gitee.com/yuandj/siger/issues/I3HTQ7#note_8117920_link)

  - [重温 ISSUE 标题，有感数学之美的文字就是最好的推介，只是迟到的期刊有些蒙尘啦。值此新年之际，推荐和乐数学，以期能点燃更多青少年追随大师的脚步，发现数学之美啊 :pray:](https://gitee.com/yuandj/siger/issues/I3HTQ7#note_8152410_link)

  - [封面](https://gitee.com/yuandj/siger/issues/I3HTQ7#note_8152476_link)【[图](https://images.gitee.com/uploads/images/2022/0104/164430_aea8c050_5631341.jpeg)】数学是最接近宇宙奥秘的科学，而数学之美无异于星辰深邃之美，故而选此背景，尽管主题 FORMAL LOGIC 只是数学之花中的一朵 :pray: 在看到 最新一期数学都知道的符号手稿，依然被吸引，并最后定稿封面，只是大师的魅力无法阻挡，他承载了数学之美的所有表现，才将原本的符号手稿主题雪藏啦。:pray:

  - [笔记全文刊发，并删除过分抢眼的图片。诚意推荐好书和优秀公号。:pray: 也是对曾经的过分功利的一个弥补 :pray:](https://gitee.com/yuandj/siger/issues/I3HTQ7#note_8152594_link)

> 特别鸣谢 “和乐数学” 的文稿，能让我近距离地体会数学之美，本期转载全部出自 “和乐数学”。

## [丘成桐自传《我的几何人生》读后感](https://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652726452&idx=2&sn=1f2731027638ee2db293e4898b0103da)

原创 科普君XueShu雪树 [和乐数学](https://images.gitee.com/uploads/images/2021/0407/235727_1614151a_5631341.png) 1周前

丘成桐自传《我的几何人生》真是好看。

当代第一流的大数学家大思想家。

第一流的数学，第一流的文笔。

有几个地方让我颇有感触。

### 第一个，百折不挠的英雄之旅。

本书开篇讲丘成桐童年的家庭贫窘。看得让人揪心，让人唏嘘。他小时候很贪玩，学习也差得很，重点中学也考不上。后来父亲去世，家道中落，颠沛流离，母亲一个人苦苦支撑。他猛然觉醒，奋发图强。柳暗花明，一路高歌猛进，百折不挠，克服艰难险阻。最后横扫数学界各大权威奖项：菲尔兹奖，克拉福德奖，沃尔夫奖，等等等等。

坎贝尔曾经总结过所有神话故事背后的英雄原型。这本书之所以过瘾，首先就在于它是一个完美的英雄崛起的励志故事。

数学是真正的千秋霸业，数学方程横穿历史超越时空，甚至能沟通星际文明。

丘成桐是我心目中真正的大英雄。

### 第二个，做大事的热情与眼界。

不管是这本传记，还是他散落各处的演讲与文章，你都能很轻易地从字里行间感受到丘成桐对做真正大学问的巨大热情。是热情与挚爱，而不是所谓的聪明。相反，甚至你不难感受到丘成桐先生身上的质朴、蛮力与笨拙。真正的聪明是大巧若拙。互联网上经常流传一些对所谓学霸如何如何聪明的肉麻吹捧，最后这些人却一无建树或成就平平。正好比吹捧一个人小学时成绩如何好。这种文章无不透着一股肤浅的小家子气。

年轻人与其看这种文章而自卑，不如多读读丘成桐，读读他的传记，演讲。你会有一种亲切感，你会受到炽热的熏陶，从而更有力，更自信。真正的大家， 首先应该能感染人，滋养人。

丘成桐身上洪流般的热情来自何处？

我想，深厚持久的文学熏陶，塑造了他的审美，后劲持久，作用甚大。

昨夜西风凋碧树，独上高楼，望尽天涯路。

他做事的眼界甚是宏大。

他做的是微分几何，却并不满足于固有的那一亩三分地，而是贪婪地吮吸和吞噬数学中的一切学问：非线性微分方程、群论、数论，等等，即使最后课堂上所有的学生都跑光了，教授给他一个人上课。衣带渐宽终不悔，为伊消得人憔悴。

> 他深知， **第一流的学问，第一流的事业，往往需要贯通原本相距遥远的事物。** 

>  **所有的珠子，最后都串成了线。** 

后来，他把微分几何和非线性微分方程融合起来，开辟了几何分析的庞大王国。

他做的是数学，却对物理有着浓厚的兴趣，譬如广义相对论、弦理论。

他向当代第一流的物理学家们学习，跟他们合作，做两个领域沟通的使者。以他的名字命名的卡丘空间，最后成为弦理论的基本工具和语言。某种程度上说，有点类似黎曼空间之于广义相对论。虽然目前还不甚完美。

他做的是数学和自然科学，却也在文学中倾注着个人的深情。

散落书中的诗词文章，可窥一斑。尽管这种形式古典内容质朴的诗文，已经远离了当今文学的主流。

没有理科与文科的人为分野。

他在乎的是更普适更深层的东西。

他在乎的是怎样做真正的大事业，做真正的大学问。

丘成桐堪为所有年轻人真正的导师。

### 第三个，科普。

这本书穿插着一些数学与物理的科普，撑起了他数学人生的骨架。当然，不是很好理解。好读书，不求甚解。初读的时候，碰到不懂的地方不必过于执着，不必锱铢必较，即使略过影响也不大，先看完一遍再说。

就科普而言，他几年前出版的另一本书《大宇之形》，详尽得多，精彩得多。当然也更难读。如果你没有阅读的强烈爱好，我不建议你读。但是如果你狠下心来硬啃，收获也会更大。你将有机会跟随着他深入到数学和物理学的最前沿，呼吸到那儿寒冽逼人的空气，感受到那儿极目万里的空阔。以后有时间我再做一番详尽的解读。

### 第四个，名利。

这本书作为传记，不可避免地谈到了学术界的名利与恩怨。以丘成桐的学术地位，如此详尽如此直率，也是罕见的。

这也挺好，让大家看到了学术界更真实更赤裸的一面，有一种祛魅的效果。

前面谈到了热情，然而想要大有成就不甘人后的雄心壮志，本身也是这种热情至关重要不可分割的一部分。

在这一点上，学术界与其他领域并无很大的差异。

只是儒家文化的熏陶，让我们更含蓄。

但是志在必得的年轻学子，内心深处，你可以理直气壮地面对它拥抱它。

书中讲了一个故事，很有意思。四维空间庞加莱猜想的证明者弗里德曼，在得奖之前，曾经打电话给丘成桐，愤愤不平地说，我比你更应该得菲尔兹奖，因为我的论文里提出了五个原创的思想，而你的只提出了一个。稚气未脱的话语中，透漏出的是强烈的荷尔蒙气息。这种争强好胜的精神，倒也有一种坦率的磊落，未尝不是一件好事。起码不应该干扰和影响对一个人真实成就的评价。

有个跟杨振宁齐名的理论物理学家叫霍夫特。他一把年纪苍苍老去的时候，建了一个小网站，详尽地指导年轻的学子应该学习哪些物理学领域，才能成为优秀的理论物理学家。他大大方方地告诉你，甚至鼓励你，怎样可以攀上理论物理学的最高峰或者拿到诺贝尔奖（两者并不重合）。

丘成桐当然不是完人。围绕着他有许多争议。虽然他在本书中诉说了自己的对声名的超脱，但是如此详述数学江湖的争论与纠缠，其实本身也流露出在乎。这种恩怨纠缠，读者身处其外，可以围观，并各自在心中作出各自不同的评判。尽信书不如无书。关于对撞机的争论，本质上是对未来的预判，而非事实的描述。各花入各眼，也不妨做如是观。

一代数学巨匠。一本好书。《我的几何人生》，推荐给大家。

#### 网友的精选留言：

- 《几何颂》

> 穹苍广而善美兮，何天理之悠悠。  
> 先哲思而念远兮，奚术算之久留。  
> 形与美之交接兮，心与物之融流。  
> 临新纪以展望兮，翼四力以真求。  
> 岂原爆之非妄兮，实万物之始由。  
> 曲率浅而达深兮，时空坦而寡愁。  
> 曲率极而物毁兮，黑洞冥而难求。  
> 相迁变而规物兮，几何雅而远谋。  
> 扬规范之场论兮，拓扑衰而复留。  
> 惟对称之内蕴兮，类不变而久悠。  
> 道深奥而动心兮，惟精析之能图。  
> 质与量之相成兮，匪线化之能筹。
>
> 登高怀书剑，  
> 携手笑扬眉。  
> 丘成桐诗一首

- 丘老在介绍这本书时曾说过有很多事情没法写进书里，实际上有一些已经在媒体公开的内容书中也没有写。我想如果把所有事情的细节都写出来，书的厚度至少要增加一倍，也更有历史价值。不知丘老能否先口述历史录音保存，待将来自己去世后再公开，这样就不会给自己造成困扰，也给后人留下更宝贵的研究素材。

---

## 数学都知道 —— arXiv专辑（2022.01.02）
原创 蒋迅 和乐数学 2022-01-02 16:17


点击上方蓝字“和乐数学”关注我们，查看更多历史文章。设为星标，快速读到最新文章。

### 1. 带有塑性单位球的巴拿赫空间的两个新例子
Two new examples of Banach spaces with a plastic unit ball
https://arxiv.org/abs/2111.10122

本文证明了巴拿赫空间和，对严格凸和，带有塑性单位球（我们称一个距离空间是塑性的，如果从这个空间到自身的每个非膨胀双射都是一个等距）。

### 2. v-回文：与回文的类比
v-palindromes: an analogy to the palindromes
https://arxiv.org/abs/2111.10211

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/134201_9f7bffc7_5631341.png "屏幕截图.png")

2007年，笔者在汽车牌照上发现了一个有趣的数字198。也就是说，如果我们取 198 及其反转 891，对每个数进行素因数分解，并将出现在每个因数分解中的数相加，则两个和都是 18。这些数字在 2018 年发表的一篇短文中正式介绍。这些数字后来被命名为 v- 回文，因为它们可以被视为与通常的回文的类比。在本文中，我们引入了 以 基中的 v-回文的概念，并证明了它们在无限多个基上的存在。最后，我们收集了一些关于 v-回文的猜想。

### 3. 线性椭圆偏微分方程有限元解的网格敏感性分析
Mesh Sensitivity Analysis for Finite Element Solution of Linear Elliptic Partial Differential Equations
https://arxiv.org/abs/2111.10935

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/134213_f6552c62_5631341.png "屏幕截图.png")

本文分析了线性椭圆偏微分方程有限元解的网格敏感性。根据网格变形及其梯度获得有限元解变化的界限。此界限显示有限元解如何随网格连续变化。结果适用于任何维度和任意非结构化单纯网格、一般线性椭圆偏微分方程和一般有限元近似。

### 4. 十八世纪的数学和地图绘制
Mathematics and map drawing in the eighteenth century
https://arxiv.org/abs/2111.11245

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/134225_66827a39_5631341.png "屏幕截图.png")

我们考虑地理地图的数学理论，重点是欧拉、拉格朗日和德莱尔 18 世纪的作品。这一时期的特点是频繁使用地图，这些地图不再是通过立体投影或其变化获得的，而是通过从球体到平面的更一般的地图。更为具体地，所期待的地理地图的特征是由一组适当选择的平行线和经络，地图所需的限制地图失真的数学属性所决定的。该论文还包含一些关于数学方法在希腊古代和现代制图学中的普遍使用的注释，以及关于数学和地理学这两个领域的相互影响的一些注释。

### 5. 一个显式的自对偶
An explicit self-duality
https://arxiv.org/abs/2111.06848v1

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/134920_7fe61940_5631341.png "屏幕截图.png")

这篇文章是一篇关于特定条件中格洛腾迪克对偶性的显式形式的阐释性文章。

### 6. 随机整数多项式的伽罗瓦群和范德瓦尔登猜想
Galois groups of random integer polynomials and van der Waerden's Conjecture
https://arxiv.org/abs/2111.06507v1

本文证明了范德瓦尔登猜想。本文的主要结果是证明了范德瓦尔登的猜想，即具有“小”伽罗华群的多项式的数目也“小”。相关阅读：https://davidlowryduda.com/on-van-der-waerdens-conjecture/

### 7. 按出现顺序对混合物进行吉布斯采样：有序分配采样器
Gibbs sampling for mixtures in order of appearance: the ordered allocation sampler
https://arxiv.org/abs/2107.08380

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/134912_c4f31398_5631341.png "屏幕截图.png")

混合模型的吉布斯采样方法基于数据增强方案，该方案考虑了数据中未观察到的分区。众所周知，条件采样器会在无限混合中缓慢混合，在这种情况下，需要某种形式的截断，无论是确定性的还是随机的。在具有随机数量成分的混合物中，探索不同维度的参数空间也具有挑战性。我们通过以混合分布指导的可交换序列以随机出现顺序表达混合成分来解决这些问题。相关阅读：https://xianblog.wordpress.com/2021/11/29/ordered-allocation-sample

### 8. 陈省身对六维球面上近复结构的研究
S.-S. Chern's study of almost-complex structures on the six-sphere
https://arxiv.org/abs/1405.3405

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/134905_51b400b5_5631341.png "屏幕截图.png")

2003 年，陈省身开始研究 6 维球面上的近复结构，其想法是利用其在特殊群 G2 下著名的近复结构不变量的特殊性质。虽然他没有解决确定在 6 球面上是否存在可积的近复结构的问题，但他确实证明了一个重要的恒等式，它解决了一个有趣的类在 6 球面上的近复结构的问题。

### 9. 欧几里得宇宙
The Euclidean Universe
https://arxiv.org/abs/2111.14162

本文介绍了一种称为欧几里得宇宙的数学结构。这种结构为非阿基米德数学，特别是非标准分析提供了一个基本框架。

### 10. 短期课程：张量代数和分析及在微分几何上的应用
A short course on Tensor algebra and analysis, with application to differential geometry
https://arxiv.org/abs/2111.14633

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/134855_c6124577_5631341.png "屏幕截图.png")

本书的内容是为研究生介绍张量代数和分析。本书远非详尽无遗，而是侧重于一些主题，旨在为读者提供现代连续介质力学课程所需的主要代数工具。张量代数和分析的表示是有意在笛卡尔坐标中完成的，通常用于经典问题。然后，一整章专门介绍曲线坐标和协变和逆变分量的形式。张量理论和结果专门用于介绍曲线和曲面微分几何的一些主题。同样在这种情况下，该演示文稿主要用于连续介质力学的应用。

### 11. 从长的数学问题中提取未知数
Extracting the Unknown from Long Math Problems
https://arxiv.org/abs/2103.12048

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/134848_b880a3a0_5631341.png "屏幕截图.png")

在解决问题的过程中，理解一个人想要解决的问题是必不可少的第一步。本文提出了通过识别长数学问题规范中的未知数的任务来促进问题理解的计算方法。作者专注于概率主题。实验结果表明，学习模型在任务上产生了很好的结果，这是朝着人类可解释的、模块化的方法来理解长数学问题迈出的有希望的第一步。

### 12. 一个独特的数学长廊
A Singular Mathematical Promenade
https://arxiv.org/abs/1612.06373

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140952_7a79cd00_5631341.png "屏幕截图.png")

这既不是奇点理论的基本介绍，也不是包含许多新定理的专门论文。这本小书的目的是邀请读者踏上数学长廊。作者拜访了喜帕恰斯、牛顿和高斯，也拜访了许多当代数学家。她带大家玩一些代数、拓扑、几何、复杂分析和计算机科学。希望一些有动力的本科生和一些更高级的数学家会喜欢这些全景图。

### 13. 基本纽结和链环理论用户指南
A user's guide to basic knot and link theory
https://arxiv.org/abs/2001.01472v1

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140941_924d8df4_5631341.png "屏幕截图.png")

这篇论文是说明性的，可供学生阅读。我们定义了纽结或链环的简单不变量（link数、Arf-Casson 不变量和 Alexander-Conway 多项式），其动机是由非专家或学生可以访问的有趣结果驱动。最简单的不变量自然会出现在试图解开结或断开链接的尝试中。然后我们为最简单的不变量提供某些“skein”递归关系，这允许引入更强的不变量。我们以一种便于计算不变量本身的方式陈述 Vassiliev-Kontsevich 定理，而不仅仅是计算不变量空间的维数。不需要先决条件；我们以不妨碍直觉理解的方式给出了主要概念的严格定义。相关阅读：https://en.wikipedia.org/wiki/Whitehead_link

### 14. N友好海伦三角形
N-sociable Heronian Triangles
https://arxiv.org/abs/2112.05517

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140927_e42b8a31_5631341.png "屏幕截图.png")

本文中，海伦三角形是具有整数边长和整数面积的三角形。按维基百科，海伦三角形是具有有理数边长和有理数面积的三角形。两个海伦三角形称为友好海伦三角形，如果其中一个的面积等于另一个的周长，反之亦然。本文把这个概念推广到N-和睦海伦三角形。相关阅读：https://arxiv.org/abs/2112.07058 Spherical Heron triangles and elliptic curves

### 15. OPM，Matlab 中优化问题的集合
OPM, a collection of Optimization Problems in Matlab
https://arxiv.org/abs/2112.05636

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140919_9c01f0d8_5631341.png "屏幕截图.png")

OPM 是 CUTEst 无约束和有界约束非线性优化问题的小集合，可在 Matlab 中直接用于测试优化算法（即无需安装额外软件）。相关阅读：https://github.com/gratton7/OPM

### 16. 林格尔圆圈问题的解
A solution to Ringel's circle problem
https://arxiv.org/abs/2112.05042

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140912_1781d404_5631341.png "屏幕截图.png")

1959年，格哈德·林格尔提出了这样的问题：考虑一个有限的圆族，使得平面中的每个点最多包含在两个圆中。如果需要将相切的圆涂上不同的颜色，为圆着色所需的最少颜色数是多少？现在我们知道了：不存在这样一个最少颜色数。相关阅读：https://gilkalai.wordpress.com/2021/12/11/to-cheer-you-up-in-difficult-times-34-ringel-circle-problem-solved-by-james-davies-chaya-keller-linda-kleist-shakhar-smorodinsky-and-bartosz-walczak

### 18. 泽夫·舒斯的遗产：应用数学中的一条意想不到的道路
Zeev Schuss' legacy: an unexpected path in applied mathematics
https://arxiv.org/abs/2112.07638

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140858_7cb68e39_5631341.png "屏幕截图.png")

Z. Schuss (1937-2018) 是一位应用数学家，在渐近、随机过程、偏微分方程、建模和信号处理方面做出了多项贡献。他以其基于 WKB 和边界层分析的激活逃逸问题的原创方法而闻名，在 Springer 出版的六本书中。文本总结了他在 20 世纪、二战期间在科学、观点和个人道路上的学术方法以及他对学术界的个人贡献。

### 19. 'V' 的系谱
The Genealogy of 'V'
https://arxiv.org/abs/2012.06072

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/141018_fb8cf572_5631341.png "屏幕截图.png")

符号 用于形式逻辑中的析取是无处不在的。它从哪里来的？本文详细介绍了符号 在其历史和逻辑背景下的演变。一些消息来源说，分离作为连接命题或公式的使用是由皮亚诺引入的；其他人认为它起源于拉丁语“或”的缩写，vel。我们表明，用于分离的符号 的起源可以追溯到怀特黑德和罗素在形式逻辑中的前原理工作。

### 20. 有理直角三角形和同余数问题
Rational right triangles and the Congruent Number Problem
https://arxiv.org/abs/2112.09553

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140828_f36e5cb8_5631341.png "屏幕截图.png")

这里的同余数不是通常意义下的同余。如果一个有理数边的直角三角形的面积是正整数的话，那么这个正整数就是一个同余数。根据欧几里得的毕达哥拉斯三元组的基本公式，本文通过一个等式定义了某些同余数的有理三元组，并探索了它们的关系。

### 21. 计算机的重点是什么？一个给纯数学家的问题
What is the point of computers? A question for pure mathematicians
https://arxiv.org/abs/2112.11598

本文讨论了计算机可能很快会帮助数学家在以前没有用的领域证明定理的想法。此外，作者认为这些相同的计算机工具也将有助于进行数学交流和教学。我想这个思想应该被所有数学家接受了。如果你是一个例外，那么应该脑补一下了。

### 22. 使用机器学习预测加速戴克斯特拉的最短路径算法
Using Machine Learning Predictions to Speed-up Dijkstra's Shortest Path Algorithm
https://arxiv.org/abs/2112.11927

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140818_cb37fdaf_5631341.png "屏幕截图.png")

本文研究使用机器学习技术解决基本最短路径问题，也称为单源多目标最短路径问题 (SSMTSP)。给定一个具有非负边权重的有向图，我们的目标是计算从给定源节点到几个指定目标节点中的任何一个的最短路径。基本上，作者的想法是将机器学习方法与戴克斯特拉算法的改编版本相结合来解决这个问题：基于戴克斯特拉算法的轨迹而设计了一个神经网络，只需几次迭代即可预测最短路径距离。然后使用预测来修剪戴克斯特拉算法探索的搜索空间。至关重要的是，新方法始终计算精确的最短路径距离，即使预测不准确，并且从不使用比标准算法更多的队列操作。相关阅读：https://zh.wikipedia.org/wiki/戴克斯特拉算法

### 23. 非凸学习的外点优化
Exterior-point Optimization for Nonconvex Learning
https://arxiv.org/abs/2011.04552

本文介绍了非凸外点优化求解器 (NExOS)——一种专为约束非凸学习问题量身定制的新型一阶算法。考虑在非凸约束上最小化凸函数的问题，其中约束集上的投影是围绕局部最小值的单值。大量非凸学习问题都具有这种结构，包括（但不限于）稀疏和低秩优化问题。通过利用约束集的基础几何结构，NExOS 通过解决一系列惩罚问题并严格减少惩罚参数来找到局部最优点 NExOS 通过应用一阶算法来解决每个惩罚问题，该算法在正则条件下线性收敛到相应惩罚公式的局部最小值。

### 24. 纳维-斯托克斯方程的不连续伽辽金方法的最优误差估计
Optimal Error Estimates of a Discontinuous Galerkin Method for the Navier-Stokes Equations
https://arxiv.org/abs/2112.12414

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140812_4686cc40_5631341.png "屏幕截图.png")

本文将非连续有限元Galerkin 方法应用于瞬态 不可压缩Navier-Stokes 模型。在 -norm 中推导出速度的最佳误差估计值和 -norm 中的压力与初始数据和中的源函数  空间。这些估计是借助新的  投影和修改的斯托克斯算子在适当的破碎 Sobolev 空间和标准抛物线或椭圆对偶参数的帮助下建立的。在数据的小假设下，估计显示是一致的。然后，分析了一种基于后向欧拉法的完全离散方案，推导出了完全离散的误差估计。与速度的 -norm 和 -norm 相关的已建立的半离散误差估计压力是最佳的，比早期文章中的压力更锐利。

### 25. 一种多空间维度保留移动网格的界面
An Interface Preserving Moving Mesh in Multiple Space Dimensions
https://arxiv.org/abs/2112.11956

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140759_c79a71e6_5631341.png "屏幕截图.png")

本文提出了一种二维或更高维的界面保持移动网格算法。它直接在  维网格内解析移动  维流形，这意味着界面由移动网格单元表面的子集表示。底层网格是符合 Delaunay 属性的单纯划分。局部重新网格算法允许强烈的界面变形。本文证明了给定的算法在界面变形和重新网格化步骤后保留了界面。源自各种数值方法，数据按单元格附加到网格上。在每次重新划分网格操作之后，保留移动网格的界面通过将数据投影到新的网格单元来保留有效数据。移动网格算法的开源实现可在以下位置获得。

### 27. 私人私人信息
Private Private Information
https://arxiv.org/abs/2112.14356

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140740_55779253_5631341.png "屏幕截图.png")

在私有私有信息结构中，代理的信号不包含有关其对等方信号的信息。本文研究这些结构的信息量如何，并表征那些处于帕累托边界的结构，因为在不侵犯隐私的情况下不可能向任何代理提供更多信息。在主要应用中，本文展示了如何在不透露包含敏感信息的相关变量的任何信息的约束下以最佳方式公开有关未知状态的信息。

### 28. 倒数平方根计算的正确舍入的牛顿步骤
A Correctly Rounded Newton Step for the Reciprocal Square Root
https://arxiv.org/abs/2112.14321

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140733_c71d846f_5631341.png "屏幕截图.png")

倒数平方根是一项重要的计算，存在许多复杂的算法。一个共同的办法是使用牛顿方法来改进估计。在本文中，我们开发了一个正确舍入的牛顿步骤，可用于提高朴素计算的准确性。

### 29. 希尔伯特空间中单调算子的一个迭代方案
An iteration scheme for monotone operators in Hilbert spaces
https://arxiv.org/abs/2112.14498

本文给出了在希尔伯特空间中寻找最大单调算子的零点的迭代方案。假设操作符定义在整个空间中。如果存在，迭代将强烈收敛到一个解，否则它们趋于无穷大。作为一个应用，我们得到了希尔伯特空间中凸函数的强收敛最小化方案。

### 30. 与吉布斯现象作斗争：关于偏微分方程不连续解的有限元近似
Battling Gibbs Phenomenon: On Finite Element Approximations of Discontinuous Solutions of PDEs
https://arxiv.org/abs/1907.03429

![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/140723_60b07b40_5631341.png "屏幕截图.png")

在本文中，我们想从近似的角度阐明当使用连续和不连续有限元来逼近不连续或接近不连续的偏微分方程解时的吉布斯现象。对于简单的不连续函数，我们在不连续匹配或不匹配网格上明确计算其连续和不连续分段常数或线性投影。对于简单的不连续对齐网格情况，分段不连续近似总是好的。

