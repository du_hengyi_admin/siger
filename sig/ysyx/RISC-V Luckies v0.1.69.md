- [RISC-V Instruction Set LuckyDoku Extendsion](https://gitee.com/RV4Kids/NutShell/issues/I4SPC9#note_8726836_link) 
  (RISC-V Luckies) v0.1.69  
  - [实验课直播 2022-2-11: 袁德俊](https://gitee.com/RV4Kids/NutShell/issues/I4SPC9#note_8729590_link) 
- [单周期处理器介绍 2021-7-12: 薛臻](https://gitee.com/RV4Kids/NutShell/issues/I4SPC9)  
- [香山芯片调试直播 2022-2-06: 包云岗](https://gitee.com/RV4Kids/XiangShan/issues/I4U0TL)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0218/014744_cddb0059_5631341.png" title="luckydoku.png"></p>

> 这是一份独特的新年礼物，看 SIGer 水印下的胖虎大家就能看出。而封面主体就是本期的主角 LuckyDoku 吉祥数独啦，相同的体裁，元旦前已经赠送了一份给 包云岗 老师（是第99套），当时是即将启动的 “一生一芯” [寒假集训](readme.md)，[命题](../../RISC-V/Shinning%20Star.md)刚刚完成，寻求他的鼓励。而封面的这套，更加有意义，我和同学们的[作业](readme.md)小成了，完成了第一个小目标，设计一套数独扩展指令。今天有了个好听的名字，RISC-V Luckies，本期专题就是汇报小节，也是一份感谢和致敬。这是第一套使用 RISC-V Luckies 生成的吉祥数独，送给一生一芯的讲师 薛臻，因为整个实验课都是基于她的《[单周期处理器介绍](https://gitee.com/RV4Kids/NutShell/issues/I4SPC9)》，又都是 RISC-V 学习的起点。好的开端就是成功的一半，“中学生设计的第一颗 CPU 芯片” 一定能很快实现，包老师 :pray:。

  - 我还有一愿，能用上这颗年轻的芯，刷新一道数独难题的求解速度 “最小数独问题：九宫标准数独唯一解，最少需要17个初始值”，2012年元旦的记录是 700万 小时。在这里与老师和同学们一起求个见证! :pray:

> 本期提纲除了 英文版 [RISC-V Luckies v0.1.69](https://gitee.com/RV4Kids/NutShell/issues/I4SPC9#note_8726836_link) 的发布，还有一节翔实的 [实验课直播](https://gitee.com/RV4Kids/NutShell/issues/I4SPC9#note_8729590_link)，是效仿包老师新年加班的[香山芯片调试直播](https://gitee.com/RV4Kids/XiangShan/issues/I4U0TL)，同时将薛臻老师的单周期处理器介绍的[详解笔记](https://gitee.com/RV4Kids/NutShell/issues/I4SPC9)作为附录。相信同学们，一定能够在这样的 Step by Step 的学习环境下，快速上手 "[一生一芯](https://oscpu.github.io/ysyx/)" 的快车道，开启 RISC-V 全栈学习之旅。:pray: 不抒怀了，上菜：

# RISC-V Instruction Set LuckyDoku Extendsion (RISC-V Luckies) v0.1.69

 **LuckyDoku**  is a special question type based on the Six Houses of Sudoku, presented as a group of nine questions. Usually their answers are exactly the same, or there are exceptions also. The Patterns of these 9 questions have an auspicious meaning. Because it is special, more computing power is required so that answers can be found faster, and special conditions need to be quickly verified. Usually, the answers to the 9 questions are all the same, indicating that the energy value is the largest, which indicates that the solver is more likely to get good luck. Whether or not the author uses computing power to aid, when he shares LuckyDoku to friends alway with  good wishes,  and lingering fragrance in his hand. The game was co-designed by a community volunteer teacher from China, Dejun Yuan, and his students, who have a common wish: Pray for Health and Happiness for the Whole World Kids!  

The RISC-V instruction set gives them the ability to customize. Here's LuckyDoku's solution:

| 31:20 | 19:15 | 14:12 | 11:7 | 6:0 | bits |
|---|---|---|---|---|---|
| imm[11:0] | rs1 | funct3 | rd | opcode | I-type |

Opcode = " **lucky** " (reserved name for LuckyDoku, Real digits defines could be self-selected.) Same as I-type code grammer `addi rd,rs1,imm` There're 16 imms below, just one of  algorithm. According to diffrent solving algorithm, imms meaning could be changed, also 128bits digits will be supported in the future, and supported more patterns of sudoku, such as nine houses of sudoku. :

- 1689: Randomly generates an out-of-order queue of 1-6 based on a specific initial value;
- 1: Copy rs1 to rd;
- 2: According to op1 as the initial value, randomly generate a 1-6 out-of-order queue;
- 3: Calculate the Sudoku column in row 2 based on the first row of digits saved in op1;
- 4: Copy the 32-bit low operand of op1 to the high 32-bit of rd;
- 6: Copy the high 32-bit operand of op1 to the low 32-bit of rd;
- 5: According to block 1 and 2, block 3 is calculated; each block is an array of 2x3;
- 7: Convert two rows of digits into 2-blocks digits; 64 bits are composed of two 32-digit numbers, and the high 32 is row1;
- 8: Calculate block 4, using block 2 and block 3; there are op1 high 32 and low 32 respectively;
- 10: Transform block 4 & block 3 into Row 3 & Row 4;
- 11: Transform block 3 & block 4 into Row 3 & Row 4;
- 12: Turns the high 32 bits into a 64-bit integer with a high 32 zero;
- 13: According to block 1 & block 3, calculate the two digits of column 1 of block 5;
- 14: Calculated block 6 digits according to the 1st two digits of block 5, if not unique randomly generated;
- 15: Merge the columns 2 columns 3 of Block 1 and Block 3 into one 32-bits integer;
- 16: Using columns 2 & columns 3 and block 6, complete the last 4 digits of the block 5;

| regs | name | 63:32 | 31:24 | | 23:20 | 19:16 | 15:12 | | 11:8 | 7:4 | 3:0 | | usage |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
| x1 | ra | | | | | | | | | | | | temp |
| x5 | t0 | | | | | | | | | | | | temp |
| x6 | t1 | temp | temp | | a1 | b1 | c1 | | d1 | e1 | f1 | | line1 |
| x7 | t2 | temp | temp | | a2 | b2 | c2 | | d2 | e2 | f2 | | line2 |
||
| x28 | t3 | temp | temp | | a3 | b3 | c3 | | d3 | e3 | f3 | | line3 |
| x29 | t4 | temp | temp | | a4 | b4 | c4 | | d4 | e4 | f4 | | line4 |
||
| x30 | t5 | temp | temp | | a5 | b5 | c5 | | d5 | e5 | f5 | | line5 |
| x31 | t6 | temp | temp | | a6 | b6 | c6 | | d6 | e6 | f6 | | line6 |

Explaination:

- x1 - x31: Register index
- ra, t0-t6: The name of the register
- temp: Temporarily holds the intermediate integers
- x = a-f (column index of Sudoku coordinates)
- y = 1-6 (row index of Sudoku coordinates)

### 实验课直播

- [建立测试代码的构建环境](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8581929_link)

  - riscv64-unknown-elf-objdump inst.bin -D -b binary -m riscv:rv64
  - riscv64-unknown-elf-objdump inst_diff.bin -D -b binary -m riscv:rv64
  - nano add2.S
    -  **riscv64-unknown-elf-as**  add2.S -o add2.o -march=rv64i
    -  **riscv64-unknown-elf-ld**  add2.o -o add2.om
    -  **riscv64-unknown-elf-objcopy**  -O binary add2.om add2.bin
  - riscv64-unknown-elf-objdump add2.bin -D -b binary -m riscv:rv64
  
- [RISC-V 寄存器的命名是什么含义？x1~x31 分别对应 ra...](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8590017_link)
  
  - [5.4 RISC-V寄存器 手册速查与延展阅读](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8590201_link)
    
- [using t1-t6 作为工作用临时寄存器，并用于最后输出六宫数独的结果](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8601300_link)

- [create random lucky6 number](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8601404_link)

  - [361245](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8601445_link) 

- [functurn lucky2(op1)](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8601459_link)
  
  - [361245 -> 245361](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8601475_link)
    
- [361245 -> rand3 -> 452163](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8601513_link)
  
- add rd, rs1, rs2  
  [lucky4 move lowbits to highbits.](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8601608_link)
  
  - [defines.v](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8601609_link)
  - [id_stage.v](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8601611_link)
  - [exe_stage.v](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8601612_link)
    
- [两行转两宫](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8607683_link)
  
- [删除6个数字中已经有的数字](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8608185_link)
  
- [4个字符的乱序](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8613097_link)
  
  - [6541, 2013, 5146 中间的 2013 是调试信息。](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8613205_link)
  - [主程序流程，用于 三宫 的前两个字符的产生](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8613428_link)

- [4个字符中 挑选 2个。](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8613874_link)
  
- [gong3 的第2对数字，从另外3个里选。](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8614549_link)
  
  - [选好了，12, 并放在了正确的位置上。](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8615471_link)
    
- [GONG3 完成 （带调试信息）](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8617244_link)
  
- [计算 宫4 使用 宫3和宫2](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8617577_link)
  
  - [取得 低 32位， addi imm = 6;](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8617610_link)
  - [两行转变成两宫 addi imm = 7;](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8617674_link)
  - [修改一个BUG，早期 BITS 合成语法问题。](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8618100_link)
  - [准备好了，luckyGong4](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8618493_link)
  - [GONG4 ling3 's 3 numbers 642](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8621883_link)
  - [GONG4 ling4 's 3 numbers 351](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8622211_link)
    
- [convert 2 gongs 2 lines](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8622357_link)

- [get gongs from lines.](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8622459_link)
  
  - [exchange gong1&2 and gong3&4 to gong13&24](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8622496_link)
    
- [生成宫5的两个数字，再计算宫6.](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8622498_link)
  
  - [mix 2numbers and gong2&4](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8622507_link)
  - [succes got gong6 16,236,514](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8622550_link)
    
- [gong52 final.](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8622552_link)
  
  - [宫52 与准备 IMM](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8626751_link)
  - [发现了一个BUG，修正了：prepare 00361254-00513426 -> 61541326](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8636096_link)
    
- [final success 1st verilog creating luckydoku was borned.](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8636162_link)
  
  - [尽管这期间出现过，编译器故障，](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8636291_link)
  - [the result.](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8636397_link)
  - [前面的LUCKY能量检测，只有 5, 微调了随机策略，重新生成了一个, 能量值提升到 8,已经相当完美拉](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8636787_link)
  - [用上石榴派手工补上一道题，就可以交付啦！](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8640199_link)
  - [create .lud files. named Lucky5ysyx.lud](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8641366_link)
    
- [制作展示内容](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8641238_link)
  
  <p><img src="https://images.gitee.com/uploads/images/2022/0211/215858_309f615a_5631341.png" width="69%"></p>

  <p><img src="https://images.gitee.com/uploads/images/2022/0211/222259_6fb5e29d_5631341.gif" width="69%"></p>
    
# 单周期RISCV处理器介绍

<p><img width="39%" src="https://images.gitee.com/uploads/images/2022/0207/155219_26e9e813_5631341.png" title="1 2022-02-07 15-40-59 的屏幕截图.png"> <img width="39%" src="https://images.gitee.com/uploads/images/2022/0207/155240_37cb0b55_5631341.png" title="2 2022-02-07 15-41-03 的屏幕截图.png"></p>

薛臻
(2021/7/12)

- RISCV64I 指令集架构
- 单周期 CPU 代码框架
- 一条指令的执行过程
- 经典五级流水线
- 单周期代码的使用指南

> 1. 嗯，各位同学大家好，我是薛臻。我来介绍一下单周期 RISC-V 的示例代码。在介绍示例代码之前，我会先简要介绍一下 RISCV64I 指令集架构，接着解释一条指令，也就是 addi 指令，在示例代码中的执行过程，然后简要介绍一下经典五级流水线和单周期代码的使用指南。

<p><img width="69%" src="https://images.gitee.com/uploads/images/2022/0207/155255_ca7c1c01_5631341.png" title="3 2022-02-07 15-41-21 的屏幕截图.png"></p>

- RISC 指令集
- **六种** 指令格式
- RISCV64I 寄存器
  - PC：程序计数器，存放下一条指令的地址
  - 32个通用寄存器，1个值恒为0的x0寄存器
- 小端存储

![输入图片说明](https://images.gitee.com/uploads/images/2022/0207/163724_dfdf3781_5631341.png "2022-02-07 16-36-16屏幕截图.png")

RISCV 指令格式

> 2. 首先，RISC-V 是 RISC 指令集，具有规整的编码格式，RISC-V有 **六种** 的指令格式。因为固定的32位长度可以发现，在所有的指令格式中，源寄存器 rs1 rs2 和目标寄存器 rd 都固定在相同的位置，简化了指令译码。这体现了 RISC 指令集的优势。大家可以看到，在所有指令中，立即数 imm 的符号位( _高位_ )总是第31位。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0207/163804_31df8a58_5631341.png "2022-02-07 16-36-51屏幕截图.png")

RISCV 寄存器

> RISCV64I 有一个程序计数器 PC，用来存放下一条指令的地址，这个应该所有 cpu 都会有的一个寄存器吧，并且有32个64位的通用寄存器 X0到X31，其中X0的值恒为零。除此之外，RISCV 指令集为小端存储模式，也就是低位存放在低地址，高位存放在高地址。

<p><img width="69%" src="https://images.gitee.com/uploads/images/2022/0207/155310_a0c42ccd_5631341.png" title="4 2022-02-07 15-42-17 的屏幕截图.png"></p>

- 取指阶段：从内存中获取当前 PC 中对应的指令，同时 PC+4
- 译码阶段：解析指令，获得源/目的操作数等信息
- 执行阶段：指令对应的运算等

```
Single cycle riscv cpu
├── defines.v        // 存放常用的变量
├── exe_stage.v      // 执行阶段
├── id_stage.v       // 译码阶段
├── if_stage.v       // 取指阶段 
├── inst.bin         // 指令文件
├── regfile.v        // riscv 寄存器
├── rvcpu-test.cpp   // verilator 的仿真文件
└── rvcpu.v          // riscvCPU
```

> 3. 在这个单周期的示例代码中，我实现了三个阶段，分别是取指阶段、译码阶段和执行阶段。这三个阶段分别对应于 if_stage.v、id_stage.v、exe_stage.v，三个文件就是三个模块，除此之外 defines.v 里存放了一些常用的变量。inst.bin 是指令文件，regfile.v 是32个通用寄存器，cpp 文件用于在 verilator 中进行仿真，rvcpu，是顶层模块。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0207/191032_fbdb2e5b_5631341.png "2022-02-07 19-10-19屏幕截图.png")

> 从这个波形图中可以看出一条指令进来，这条指令执行的是 0+1 的加法操作，在同一个时钟周期内就得到了运算结果。

<p><img width="69%" src="https://images.gitee.com/uploads/images/2022/0207/155323_f42d6214_5631341.png" title="5 2022-02-07 15-43-02 的屏幕截图.png"></p>

> 4. 接下来，我以 inst.bin 中第一条指令 addi 为例，详细解释一下这条指令，在示例代码中的执行过程。我先说明具体做法，然后再结合代码进行介绍。首先，Addi 指令是一条 I 型指令，不同字段具有特定含义。比如高12位表示立即数 imm，7-11位是 目标寄存器 rd 的地址，15到19位是源寄存器 rs1 的地址等等。

 **取指** 

- **从内存取出指令** ，同时 PC+4

> 在 RISC-V cpu工作的过程中，首先cpu从内存中取出这条指令，这里是从 inst.bin 这个文件中，同时给 程序计数器 PC +4，这是取指阶段；

 **译码** 

- **解析指令** 0x0010_0093
  - imm: 1
  - rs1: x0
  - Func3: 000
  - rd: x1
  - opcode: 0010011

> 接下来进入译码阶段，译码模块解析指令，得到这条指令的这些字段：也就是 立即数 imm, rs1, Func3 和 opcode 字段。

- **识别指令** 
  - opcode&Func3 = addi

- **确定指令类型/操作码** 
  - I型指令
  - 加法操作码 INST_ADD

- **获取操作数来源** 
  - I型指令：
  - op1——寄存器
  - op2——立即数

> 5. 接下来，根据 opcode 和 Func3 字段识别出这是一条 addi 指令。得到这个关键信息后，进而可以确定这是一条 I 行指令，并且它执行加法操作，从而将其指令操作码确定为加法操作码。那既然现在已经知道了，这是一条I行指令。那么它的操作数来源就理所当然的确定下来了，那就是操作数 op1 来自于寄存器，操作数 op2 来自于立即数，

- **产生寄存器 读/写信号** 
  - r_ena1 = 1
  - r_addr1 = 0
  - r_ena2 = 0
  - w_ena = 1
  - w_addr = 1

> 同时对应的寄存器使能，与读写相关信号也被确定下来。在这条 addi 指令中，读口一的使能信号 r_ena1 = 1，表示可读，并且读地址为 s1，也就是 r_addr1=0 。读口二的使能信号 r_ena2=0，不允许读。写使能信号为一 w_ena=1，表示可写。

- **获取操作数 op1, op2** 

 **执行** 

- **根据指令操作码，执行相应操作** 
  - INST_ADD:
  - 进行加法计算 op1+op2

> 6. 此时可以通过读寄存器和立即数的符号扩展，分别得到操作数一 op1 和操作数二 op2，他们被传送到执行模块用来进行运算。执行模块，根据接收到的指令操作码来进行相应的操作。在这条指令中，操作码是加法操作码，因此执行加法操作也就是 0+1，得到的结果为 1。

<p><img width="69%" src="https://images.gitee.com/uploads/images/2022/0207/155340_1abb2edd_5631341.png" title="6 2022-02-07 15-45-06 的屏幕截图.png"></p>

 **取指** if_stage.v

```
always@( posedge clk )
begin
  if( rst == 1'b1 )
  begin
    pc <= `ZERO_WORD ;
  end
  else
  begin
    pc <= pc + 4;
  end
end
```

> 7. 嗯对应代码，我们再来看一遍。首先在取值模块中，每过一个时钟周期，程序计数计PC的值加四。

**译码：解析指令** id_stage.v

```
assign opcode = inst[6  :  0];
assign rd     = inst[11 :  7];
assign func3  = inst[14 : 12];
assign rs1    = inst[19 : 15];
assign imm    = inst[31 : 20];
```

> 进入译码模块后，译码模块通过取出指令的不同字段，实现了对指令的解析。

 **译码：识别指令** 

```
wire inst_addi =   ~opcode[2] & ~opcode[3] & opcode[4] & ~opcode[5] & ~opcode[6]
                 & ~func3[0] & ~func3[1] & ~func3[2];
```

> 然后根据 opcode 高5位和 Func3 确定这是一条addi 指令。不考察 opcode 的低两位是因为在 RISC-V 中所有 opcode 的低两位均为 11。

 **译码：确定类型/操作码** 

```
// arith inst: 10000; logic: 01000;
// load-store: 00100; j: 00010;  sys: 000001
assign inst_type[4] = ( rst == 1'b1 ) ? 0 : inst_addi;

assign inst_opcode[0] = (  rst == 1'b1 ) ? 0 : inst_addi;
assign inst_opcode[1] = (  rst == 1'b1 ) ? 0 : 0;
assign inst_opcode[2] = (  rst == 1'b1 ) ? 0 : 0;
assign inst_opcode[3] = (  rst == 1'b1 ) ? 0 : 0;
assign inst_opcode[4] = (  rst == 1'b1 ) ? 0 : inst_addi;
assign inst_opcode[5] = (  rst == 1'b1 ) ? 0 : 0;
assign inst_opcode[6] = (  rst == 1'b1 ) ? 0 : 0;
assign inst_opcode[7] = (  rst == 1'b1 ) ? 0 : 0;
```

> 因为这是一条 addi 指令，所以可以得到这是一条 I 型指令，并且其指令操作码为加法操作码。在这个示例代码中，加法操作码为 11，这是我自己规定的，你也可以规定别的值，只要保证不同操作的操作码不同即可。

 **译码：产生寄存器读/写信号** 

```
assign rs1_r_ena  = ( rst == 1'b1 ) ? 0 : inst_type[4];
assign rs1_r_addr = ( rst == 1'b1 ) ? 0 : ( inst_type[4] == 1'b1 ? rs1 : 0 );
assign rs2_r_ena  = 0;
assign rs2_r_addr = 0;

assign rd_w_ena   = ( rst == 1'b1 ) ? 0 : inst_type[4];
assign rd_w_addr  = ( rst == 1'b1 ) ? 0 : ( inst_type[4] == 1'b1 ? rd  : 0 );
```

> 因为这是一条I型指令，因此寄存器读口1可读，读口2不可读，写口使能为1，因此寄存器读口，

 **译码：获取操作数** 

```
assign op1 = ( rst == 1'b1 ) ? 0 : ( inst_type[4] == 1'b1 ? rs1_data : 0 );
assign op2 = ( rst == 1'b1 ) ? 0 : ( inst_type[4] == 1'b1 ? { {52{imm[11]}}, imm } : 0 );
```

> 8. 因此译码模块通过寄存器的读数据和立即数的符号扩展，得到操作数1和操作数2。

 **执行** 

```
    case( inst_opcode )
	  `INST_ADD: begin rd_data = op1 + op2;  end
	  default:   begin rd_data = `ZERO_WORD; end
	endcase
```

> 在执行阶段，执行模块发现接收到的指令操作码为加法操作码，于是执行加法计算，从而得到计算结果。

- 单周期 RISCV CPU：在一个周期内得到执行结果
- 取出指令后，其译码、执行过程均为组合逻辑
- 思考：如果是五级流水线，得到执行结果需要几个时钟周期？需要怎样添加时序逻辑？

> 需要强调一下，这是一个单周期的 RISCV cpu，他在取出指令后，译码和执行过程均为组合逻辑，因此在一个周期内就得到了执行结果。那在一生一芯中，我们要求实现五级流水线，那么，在这种情况下，得到执行结果需要几个周期？应该怎样添加时序逻辑呢？这个留给大家去思考。

<p><img width="69%" src="https://images.gitee.com/uploads/images/2022/0207/155352_fa2a8789_5631341.png" title="7 2022-02-07 15-46-42 的屏幕截图.png"></p>

- **IF: 取指**  将指令从存储器中读出
- **ID: 译码**  翻译指令，得到源/目的操作数的相关信息
- **EX: 执行**  执行指令对应的操作
- **MEM: 访存**  对存储器进行读/写
- **WB: 写回**  将执行结果写入通用寄存器

流水线：通过提高 CPU 中 **各个部件的利用率** ，提高其工作效率

> 9. 接下来简要介绍一下经典的五级流水线，他的五个阶段分别是取指、译码、执行、访存和写回。其中，取指是将指令从存储器中读出，译码阶段翻译指令，从而得到原操作数和目的操作数的相关信息。执行阶段用来执行指令对应的操作，比如刚才的加法。访存实现对存储器的读写，而写回阶段则将执行结果写入通用寄存器。在处理器中，流水线通过提高 **各个部件的利用率** 来提高工作效率。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0207/230711_2a365398_5631341.png "2022-02-07 23-06-55屏幕截图.png")

> 10. 嗯，举个例子，对于指令1、指令2和指令3，在顺序执行的模式下，每个指令需要五个周期，那么三条指令就需要15个周期。如果使用流水线的执行模式，那么三条指令，只需要七个周期就可以完成，加速比是15除以7达到2倍多。在顺序执行时，取指、译码、执行、访存、写回依次进行。每个周期只有一个部件在进行工作。而在流水线模式下，指令1在执行阶段时，同时指令2在译码，同时指令3在取值。所以说流水线是通过提高cpu中部件的利用率来提高工作效率的。

<p><img width="69%" src="https://images.gitee.com/uploads/images/2022/0207/155409_dbf25ce0_5631341.png" title="8 2022-02-07 15-47-50 的屏幕截图.png"></p>

 **单周期代码使用指南** 

- 只实现了取指、译码和执行阶段， **请同学们完成访存和写回阶段** 
- 添加时序逻辑， **实现五级流水线** 
- **思考：** 
  - **数据相关：** RAW（写后读），WAR（读后写），WRW（写后写）哪种需要关注？
  - **控制相关：** 分支指令如何处理？
  - 流水线 **暂停（stall）** 、流水线 **删除（kill）** 怎样实现？
- 单周期代码仅供参考

 **实现访存、写回阶段后，将寄存器写使能、写地址和写数据逐级下传到写回阶段** 

 **id_stage.v** 

```
assign rd_w_ena   = ( rst == 1'b1 ) ? 0 : inst_type[4];
assign rd_w_addr  = ( rst == 1'b1 ) ? 0 : ( inst_type[4] == 1'b1 ? rd  : 0 );
```

 **exe_stage.v** 

```
output reg  [`REG_BUS]rd_data
```

 **功能验证流程** 

- 实现新指令
- 在AM上编译得到.bin测试文件
- Verilator 仿真验证
- Difftest 调试

> 11. 最后我说明一下单周期代码的使用指南。这个示例代码只实现了取指、译码和执行， **请同学们完成访存和写回阶段** 的实现。刚才在介绍五级流水线的时候说过，写回阶段会将计算结果写回到寄存器中，所以在同学们实现了访存和写回模块中，请将寄存器读使能、读地址和读数据信号向后逐级下传，传到写回阶段，并在写回阶段进行寄存器的写操作。还有，请在在适当的地方添加时序逻辑， **实现五级流水线** 。最后请 **思考** 在三种数据相关中，我们需要重点关注哪一种，是写后读读后写还是写后写？另外再遇到分支和跳转指令时应该怎样处理，什么情况下需要流水线 **暂停** 和流水线 **删除** ？嗯，他们应该怎样实现？最后，最后需要说明一下，单周期示例代码，仅供参考，欢迎大家一起讨论。

<p><img width="69%" src="https://images.gitee.com/uploads/images/2022/0207/155419_3fe37d5b_5631341.png" title="9 2022-02-07 15-48-47 的屏幕截图.png"></p>

好，谢谢大家。

# [香山芯片调试直播](https://zhuanlan.zhihu.com/p/463270236)

 **包云岗** 
​
​中国科学院计算技术研究所 研究员

2021年6月22日，香山在RISC-B中国峰会上第一次亮相，这是当时公开的国际上性能最高的开源RISC-V处理器核设计，受到国内外的很多关注，在全球最大的开源项目托管网站GitHub上不到3个月就有近2000个Star。虽然我们的报告是中文的，但却有不少英文报道，甚至还有来自俄罗斯的关注。

2021年7月15日，第一代香山“雁栖湖”流片。但接下来由于受到全球芯片产能影响，我们不得不经历漫长的等待期。因为许久没有回片后的消息，有一些关注香山的朋友发来小心翼翼的询问：“香山是不是流片失败了？”流片失败，就是指香山无法点亮，也就意味着它就是一块石头。

2022年1月20日，等待了整整半年，香山终于回片了。此时，采用香山枫叶红底色的主板早就虚位以待。

因为疫情，香山团队的多位小伙伴决定留在北京攻关调试。除了这些这些线下的小伙伴，还有全国各地的老师和同学一起在线参与。大家非常给力，每天都有大进展：CPU频率＞1GHz，DDR4-2400稳定运行，Linux正常启动，CoreMark成功运行。 **整个芯片和板卡没有一处错误，团队有多位资深专家，大家都表示调试从来没有这么顺利过。** 

整个芯片和板卡没有一处错误，团队有多位资深专家都表示调试从来没有这么顺利过。

春节将至，香山团队的多位小伙伴在家人的支持下，决定留在北京攻关调试。除了这些这些线下的小伙伴，还有全国各地的老师和同学一起在线参与。大家非常给力，每天都有大进展：CPU频率＞1GHz，DDR4-2400稳定运行，Linux正常启动，CoreMark成功运行。整个芯片和板卡没有一处错误，团队有多位资深专家，大家都表示调试从来没有这么顺利过。

接下来把SPECCPU 2006分值跑出来，就完成这次攻关调试目标了。本来以为要用整个春节，看来攻关目标年前就可以提前实现了。

 **2022.2.5更新：** 教训——不要高兴太早！在运行SPEC CPU2006时，发现DDR4-2400稳定性还是存在问题，会导致返回数据出现随机错误。这是CPU芯片调试过程最常见的的硬骨头，终究是要面对。

图1：稍有点乱的调试攻关作战室

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014625_89c24ea7_5631341.png "屏幕截图.png")

图2：每日攻关任务清单

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014631_f6d7622a_5631341.png "屏幕截图.png")

图3：DDR4-2400内存稳定性测试

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014653_183f76c9_5631341.png "屏幕截图.png")

图4：Linux启动进入shell

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014658_37622d66_5631341.png "屏幕截图.png")

图5：运行CoreMark

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014716_6f84092f_5631341.png "屏幕截图.png")

图6：小伙伴们在讨论问题

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014722_44b5a077_5631341.png "屏幕截图.png")

图7：围观调试中

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014728_88813043_5631341.png "屏幕截图.png")

图8：小伙伴在努力思考

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014736_6829bf33_5631341.png "屏幕截图.png")

图9-11：唐丹老师和李作骏同学讨论DDR初始化

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014743_c3d1c455_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014752_bb3d0d47_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014758_b787adee_5631341.png "屏幕截图.png")

图12：两位快乐的同学徐易难与甄好

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014814_a1e7ea14_5631341.png "屏幕截图.png")

图13：提前准备的床垫和被子，看来用不上了

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014824_b7d08312_5631341.png "屏幕截图.png")

图14：后勤要准备好，吃喝管好

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014832_98b3977d_5631341.png "屏幕截图.png")

图15：专门采购了一台PS5，该放松时要放松

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014839_1b3afbd3_5631341.png "屏幕截图.png")

图16：孙凝晖院士赠予的春联

![输入图片说明](https://images.gitee.com/uploads/images/2022/0217/014851_4071c729_5631341.png "屏幕截图.png")

视频：调试现场

[香山芯片调试直播](https://www.zhihu.com/zvideo/1471267823649959936)

包云岗的视频 · 7013 播放

感谢所有关注和关心香山的朋友们！谢谢您的支持、鼓励和帮助！

提前祝大家虎年新春快乐！

 **更新：** 评论区有热烈的讨论，有很多朋友对香山调试工作给予鼓励和支持，也有朋友对春节留守调试表示质疑与不解。在此，我们一起表示感谢！谢谢大家的关心、理解、支持和建议！

每一个决策都需要信息，信息不对称是普遍的，大家面对的信息不同，从而产生不同的决策，是可以理解的。前面文章中并没有给出我们为何做成春节留守调试香山这个决策的依据，但我们团队讲究实事求是，不会为了作秀做出这样的决策，更不是要压榨香山团队成员而做出这样的决策。我们确实面临时间很紧迫的挑战，这里可以稍微多补充一些信息：

（1）香山去年7月15号便流片，正常11月份就能回片。但去年因为全球封装基板产能问题，我们等了足足半年多的时间，直到1月下旬才拿到香山芯片。

（2）按原计划是2021年12月份就要准备项目结题验收，但回片已经是1月下旬，时间已经严重滞后。好在项目监理也理解现在全球流片回片慢的现状，允许推迟几个月验收。

（3）硬件调试存在很大不确定性，很可能1个bug就会需要耗上1-2个月才能解决。而硬件问题解决不了，软件调试就无法开展。尽早发现硬件问题，把硬件调稳定，对整个项目的调试工作意义重大。说实话，我们是希望尽早消除一些不确定性，否则过年心里也不踏实。

（4）近期北京出现疫情，再加上冬奥会、等一系列活动，疫情防控存在很大不确定性。大家1月份离京后，如果其他地方再出现零星疫情，很可能一段时间内甚至整个2月不能返京，那就会导致再空耗一个月。

打个比方，这就好比身体不舒服去医院做了检查，年前报告回来了，但要求现在不能看，要等春节假期结束后才能看报告。这种心情和我们拿到香山板卡春节后再开始调试是一样的。

 **时间，对于我们来说真的很宝贵！** 

最后，再次感谢大家的支持！

发布于 2022-01-30 23:52